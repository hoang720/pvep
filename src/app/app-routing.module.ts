import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [ 
  { path: '', loadChildren: () => import('./pages/auth/login/login.module').then(m => m.LoginModule) },
  { path: 'admin', loadChildren: () => import('./layout/layout.module').then(m => m.LayoutModule) },
  { path: 'user', loadChildren: () => import('./pages/user/user.module').then(m => m.UserModule) },
  { path: 'thong-ke', loadChildren: () => import('./pages/thong-ke/thong-ke.module').then(m => m.ThongKeModule) },
  { path: 'danh-muc', loadChildren: () => import('./pages/danh-muc/danh-muc.module').then(m => m.DanhMucModule) },
  { path: 'truy-van-data', loadChildren: () => import('./pages/truy-van-data/truy-van-data.module').then(m => m.TruyVanDataModule) },
  { path: 'file-detail', loadChildren: () => import('./pages/danh-muc/file-detail/file-detail.module').then(m => m.FileDetailModule) },
  { path: 'truc-quan-hoa', loadChildren: () => import('./pages/truc-quan-hoa/truc-quan-hoa.module').then(m => m.TrucQuanHoaModule) },
  { path: 'data-khai-thac', loadChildren: () => import('./pages/data-khai-thac/data-khai-thac.module').then(m => m.DataKhaiThacModule) },
  { path: 'welllogs', loadChildren: () => import('./pages/welllogs/welllogs.module').then(m => m.WelllogsModule) },
  { path: 'phan-quyen', loadChildren: () => import('./pages/phan-quyen/phan-quyen.module').then(m => m.PhanQuyenModule) },
  { path: 'data-hinh-anh', loadChildren: () => import('./pages/data-hinh-anh/data-hinh-anh.module').then(m => m.DataHinhAnhModule) },
  { path: 'chuan-hoa-du-lieu', loadChildren: () => import('./pages/chuan-hoa-du-lieu/chuan-hoa-du-lieu.module').then(m => m.ChuanHoaDuLieuModule) },
  { path: 'du-bao-khai-thac', loadChildren: () => import('./pages/du-bao-khai-thac/du-bao-khai-thac.module').then(m => m.DuBaoKhaiThacModule) },
  { path: 'du-bao-khai-thac-main', loadChildren: () => import('./pages/du-bao-khai-thac-main/du-bao-khai-thac-main.module').then(m => m.DuBaoKhaiThacMainModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
