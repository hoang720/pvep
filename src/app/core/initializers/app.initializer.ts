import { AuthenticationService } from "../services/authentication.service";

export function appInitializer(authService: AuthenticationService) {
  return () => {
    new Promise((resolve:any, reject) => {
      //else check previous mail login then check access token that saved in local storage
      //get access token from localStorage
      let accessToken = authService.getAccesTokenFromLocalStorage();

      //if not null get refresh token and get user infor
      if (accessToken) {
        let user =  authService.getUserFromLocalStorage()
        authService.setUser(user)
        resolve()
      } else resolve();
    });
  };
}
