import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import {
  MOODLE_CURRENT_USE,
  MOODLE_JWT_TOKEN,
  USER_KEY,
} from '../config/constant';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  returnURL: string;

  private userSubject = new BehaviorSubject(null);
  private user = this.userSubject.asObservable();

  constructor(
    private router: Router,
    private http: HttpClient,
    private route: ActivatedRoute
  ) {
    this.returnURL = this.route.snapshot.queryParams['returnURL'] || '/';
  }

  ngOnDestroy() {
    this.stopRefreshTokenTimer();
  }
  setUser(UserData: any) {

    this.userSubject.next(UserData);
  }
  getUser() {
    return this.user;
  }
  get userValue(): any {
    return this.userSubject.value;
  }
  login(loginDataValue: { username: string; password: string }) {
    return this.http
      .post<any>(`${environment.apiURL}/auth/login`, loginDataValue, {
        withCredentials: true,
      })
      .pipe(
        map((res: any) => {
          this.setUser(res);
          this.saveUserToLocalStorage(res)
          this.saveAccessTokenInLocalStorage(res.access_token);
          return res;
        })
      );
  }
  logout() {
    this.stopRefreshTokenTimer();
    this.setUser(null);
    this.removeUserFromLocalStorage()
    this.router.navigateByUrl('/');
  }
  saveAccessTokenInLocalStorage(accessToken: string) {
    let accessTokenEncode = btoa(accessToken);
    localStorage.setItem(MOODLE_JWT_TOKEN, JSON.stringify(accessTokenEncode));
  }

  saveUserToLocalStorage(user: any) {

    let userJson =  JSON.stringify(user)

    localStorage.setItem(USER_KEY,userJson);
  }

  getUserFromLocalStorage() {
    let userJson = localStorage.getItem(USER_KEY);
    if (userJson) {

      let user =JSON.parse(userJson)
      return user
    } else return null;
  }

  removeUserFromLocalStorage = ()=>{localStorage.removeItem(USER_KEY)}

  getAccesTokenFromLocalStorage() {
    const accessTokenEncodeJson = localStorage.getItem(MOODLE_JWT_TOKEN);
    if (accessTokenEncodeJson != null) {
      let accessTokenEncode = JSON.parse(accessTokenEncodeJson);
      return atob(accessTokenEncode);
    }
    return null;
  }

  private refreshTokenTimeout: any;
  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }
}
