import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ChuanHoaDataService {
  private url = environment.apiURL
  constructor(private http: HttpClient) { }

  getPipe(): Observable<any>{
    let apiURL = `${this.url}/transform-pipe/`;
    return this.http.get<any>(apiURL)
  }
  getBYname(name: any): Observable<any>{
    let apiURL = `${this.url}/transform-pipe/${name}`;
    return this.http.get<any>(apiURL)
  }
  chart(name: any): Observable<any>{
    let apiURL = `${this.url}/transform-pipe/${name}/history`;
    return this.http.post(apiURL, {})
  }
  addPipe(pipe: any, name: any): Observable<any>{
    let apiURL = `${this.url}/transform-pipe/?name=${name}`;
    return this.http.post(apiURL, pipe)
  }
  runPipe(name: any): Observable<any>{
    let apiURL = `${this.url}/transform-pipe/${name}/run`;
    return this.http.post(apiURL, {})
  }
  deletePipe(name: any): Observable<any>{
    let apiURL = `${this.url}/transform-pipe/${name}`;
    return this.http.delete(apiURL)
  }


  getPreprocess(): Observable<any>{
    let apiURL = `${this.url}/preprocess-pipe/`;
    return this.http.get<any>(apiURL)
  }
  getPreBYname(name: any): Observable<any>{
    let apiURL = `${this.url}/preprocess-pipe/${name}`;
    return this.http.get<any>(apiURL)
  }
  addPreprocess(pre: any, name: any): Observable<any>{
    let apiURL = `${this.url}/preprocess-pipe/?name=${name}`;
    return this.http.post(apiURL, pre)
  }
  runPreprocess(name: any): Observable<any>{
    let apiURL = `${this.url}/preprocess-pipe/${name}/run`;
    return this.http.post(apiURL, {})
  }
  deletePreprocess(name: any): Observable<any>{
    let apiURL = `${this.url}/preprocess-pipe/${name}`;
    return this.http.delete(apiURL)
  }
}
