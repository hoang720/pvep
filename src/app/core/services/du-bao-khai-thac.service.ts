import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class DuBaoKhaiThacService {
  private url = environment.apiURL
  constructor(private http: HttpClient) { }

  getModel(): Observable<any>{
    let apiURL = `${this.url}/model-pipe/`;
    return this.http.get<any>(apiURL)
  }
  getModelName(modelName: any): Observable<any>{
    let apiURL = `${this.url}/model-pipe/${modelName}`;
    return this.http.get<any>(apiURL)
  }
  addModel(model: any, modelName: any): Observable<any>{
    let apiURL = `${this.url}/model-pipe/?name=${modelName}`;
    return this.http.post(apiURL, model)
  }
  runModel(modelName: any): Observable<any>{
    let apiURL = `${this.url}/model-pipe/${modelName}/run`;
    return this.http.post(apiURL, {})
  }
  chartModel(modelName: any): Observable<any>{
    let apiURL = `${this.url}/model-pipe/${modelName}/history`;
    return this.http.post(apiURL, {})
  }
  deleteModel(modelName: any): Observable<any>{
    let apiURL = `${this.url}/model-pipe/${modelName}`;
    return this.http.delete(apiURL)
  }
}
