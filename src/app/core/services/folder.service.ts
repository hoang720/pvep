import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FolderService {
  private url = environment.apiURL
  constructor(private http: HttpClient) { }

  getFolderByName(folderName: any): Observable<any> {
    let apiURL = `${this.url}/folder/?name=${folderName}`;
    return this.http.get<any>(apiURL)
  }

  getFileByID(id: string): Observable<any> {
    let apiURL = `${this.url}/folder/${id}`;
    return this.http.get<any>(apiURL)
  }

  getAll(): Observable<any> {
    let apiURL = `${this.url}/folder/root`;
    return this.http.get<any>(apiURL)
  }

  getAllSUbFolder(path: any): Observable<any> {
    let apiURL = `${this.url}/folder/children?path=${encodeURIComponent(path)}`
    return this.http.get<any>(apiURL)
  }

  addFolder(folder: any): Observable<any> {
    let apiURL = `${this.url}/folder/`;
    return this.http.post<any>(apiURL, folder)
  }

  updateFolder(folder: any, id: string): Observable<any> {
    let apiURL = `${this.url}/folder/${id}`
    return this.http.put<any>(apiURL, folder)
  }

  uploadFolder(folder: any, path: any) {
    var formData: any = new FormData();
    formData.append("file", folder)
    let apiURL = `${this.url}/upload_file/upload?path=${encodeURIComponent(path)}`
    return this.http.post<any>(apiURL, formData).pipe(first());
  }

  deleteFolder(id: string): Observable<any> {
    let apiURL = `${this.url}/folder/${id}`
    return this.http.delete<any>(apiURL)
  }
}
