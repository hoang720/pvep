import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NguoiDungService {

  private url = environment.apiURL
  private urlImage = environment.apiImage
  constructor(private http: HttpClient) { }

  addAvatar(avata: any):Observable<any>{
    var formData: any = new FormData();
    formData.append("uploaded_file", avata);
    return this.http.post<any>(`${this.urlImage}`,formData).pipe(first());
  }

  getUser(): Observable<any> {
    let apiURL = `${this.url}/user/`
    return this.http.get<any>(apiURL)
  }

  addUser(
    User: any,
    username: any,
    password: any,
    fullname: any,
    avata: any,
    birday: any,
    gender: any,
    sdt: any,
    email: any,
    role: any,
    status: any
  ): Observable<any> {
    let apiURL = `${this.url}/user/?username=${username}&password=${password}
    &fullname=${fullname}&avata=${avata}&birday=${birday}&gender=${gender}&sdt=${sdt}&email=${email}&role=${role}&status=${status}`;
    return this.http.post<any>(apiURL, User)
  }

  updateUser(
    User: any,
    id: string,
    username: any,
    fullname: any,
    avata: any,
    birday: any,
    gender: any,
    sdt: any,
    email: any,
    role: any,
    status: any
  ) {
    let apiURL = `${this.url}/user/${id}?username=${username}&fullname=${fullname}&avata=${avata}&birday=${birday}&gender=${gender}&sdt=${sdt}&email=${email}&role=${role}&status=${status}`;
    return this.http.put<any>(apiURL, User)
  }

  deleteUser(id: string): Observable<any>{
    let apiURL = `${this.url}/user/${id}`;
    return this.http.delete<any>(apiURL)
  }
}
