import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class PhanQuyenService {
  private url = environment.apiURL
  constructor(private http: HttpClient) { }

  getAllQuyen(): Observable<any>{
    let apiURL = `${this.url}/role/GetAll/`;
    return this.http.get<any>(apiURL)
  }

  getDacQuyen(): Observable<any>{
    let apiURL = `${this.url}/privilege/GetAll`;
    return this.http.get<any>(apiURL)
  }

  getQuyenByName(name: any): Observable<any>{
    let apiURL = `${this.url}/role/?name=${name}`;
    return this.http.get<any>(apiURL)
  }

  addQuyen(quyen: any):Observable<any>{
    let apiURL = `${this.url}/role/`;
    return this.http.post<any>(apiURL, quyen)
  }

  updateQuyen(quyen: any, id: string): Observable<any>{
    let apiURL = `${this.url}/role/${id}`;
    return this.http.put<any>(apiURL, quyen)
  }

  deleteQuyen(id: string): Observable<any>{
    let apiURL = `${this.url}/role/${id}`;
    return this.http.delete<any>(apiURL)
  }
}
