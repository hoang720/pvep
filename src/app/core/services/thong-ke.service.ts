import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ThongKeService {
  private url = environment.apiURL
  constructor(private http: HttpClient) { }

  sizeTotal(): Observable<any> {
    let apiURL = `${this.url}/folder/size_total`
    return this.http.get<any>(apiURL)
  }

  countTotal(): Observable<any> {
    let apiURL = `${this.url}/folder/count_total`
    return this.http.get<any>(apiURL)
  }

  chartData(): Observable<any> {
    let apiURL = `${this.url}/folder/nb_files`
    return this.http.get<any>(apiURL)
  }

  filesSize(): Observable<any> {
    let apiURL = `${this.url}/folder/size_files`
    return this.http.get<any>(apiURL)
  }
}
