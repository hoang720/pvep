import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TrucQuanHoaService {

  private url = environment.apiURL
  constructor(private http: HttpClient) { }

  getListWellog(name: any): Observable<any>{
    let apiurl = `${this.url}/query/welllogs/listfiles/?collection=${encodeURIComponent(name)}`;
    return this.http.get<any>(apiurl)
  }

  getData(filename: any): Observable<any>{
    let apiurl = `${this.url}/query/hist-prod/visualize/?filename=${encodeURIComponent(filename)}`;
    return this.http.get<any>(apiurl)
  }

  getWelllogs(welllogSelected: any): Observable<any>{
    let apiurl = `${this.url}/query/welllogs/visualize/?filename=${encodeURIComponent(welllogSelected)}`;
    return this.http.get<any>(apiurl)
  }

  getFilterWelllogs(filename: any): Observable<any>{
    let apiurl = `${this.url}/query/welllogs/missing-stats/?filename=${encodeURIComponent(filename)}`;
    return this.http.get<any>(apiurl)
  }

  getScatterChart(filename: any): Observable<any>{
    let apiurl = `${this.url}/query/welllogs/visualize/scatter?filename=${encodeURIComponent(filename)}`;
    return this.http.get<any>(apiurl)
  }
  getHistogramChart(filename: any): Observable<any>{
    let apiurl = `${this.url}/query/welllogs/visualize/histogram?filename=${encodeURIComponent(filename)}`;
    return this.http.get<any>(apiurl)
  }
  getSumChart(filename: any): Observable<any>{
    let apiurl = `${this.url}/query/welllogs/visualize/sum?filename=${encodeURIComponent(filename)}`;
    return this.http.get<any>(apiurl)
  }

  getImage(page: any, page_size: any): Observable<any>{
    let apiurl = `${this.url}/folder/pictures?page=${page}&page_size=${page_size}`;
    return this.http.get<any>(apiurl)
  }
  getImageName(imageName: any): Observable<any>{
    let apiurl = `${this.url}/folder/pictures?name=${imageName}`;
    return this.http.get<any>(apiurl)
  }

  
}
