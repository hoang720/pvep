import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class TruyVanService {
  private url = environment.apiURL
  constructor(
    private http: HttpClient
  ) { }

  getQuery(sql_query: any, page: any, page_size: any): Observable<any>{
    let apiurl = `${this.url}/query?sql_query=${sql_query}&page=${page}&page_size=${page_size}`
    return this.http.get<any>(apiurl)
  }
}
