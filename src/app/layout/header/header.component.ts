import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from 'src/app/core/services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() ToggleSidebar: EventEmitter<any> = new EventEmitter();
  myDate: Date | undefined;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void {
    this.dayTime()
  }

  toggleSidebar(){
    this.ToggleSidebar.emit()
  }
  Logout(){
    this.authenticationService.logout()
  }

  dayTime(){
    setInterval(() => {
      this.myDate = new Date()
    }, 1000)
  }
}
