export const sidemenu = {
  Thông_tin_hệ_thống:{
    name:"Thông tin hệ thống",
    path:'thong-ke',
    icon: 'nav-icon fas fa-server'
  },
  Quản_lý_tài_nguyên: {
    name: 'Quản lý tài nguyên',
    path: 'danh-muc',
    icon: 'nav-icon fas fa-list',
  },
  Truy_vấn_dữ_liệu: {
    name: 'Truy vấn dữ liệu',
    path: 'truy-van-data',
    icon: 'nav-icon fas fa-database'
  },
  Trực_quan_hóa_dữ_liệu: {
    name: 'Trực quan hóa dữ liệu',
    path: 'truc-quan-hoa',
    icon: 'nav-icon fas fa-eye'
  },
  Chuẩn_hóa_dữ_liệu: {
    name: 'Chuẩn hóa dữ liệu',
    path: 'chuan-hoa-du-lieu',
    icon: 'nav-icon fas fa-gears'
  },
  Huấn_luyện_mô_hình: {
    name: 'Huấn luyện mô hình',
    path: 'huan-luyen-mo-hinh',
    icon: 'nav-icon fas fa-bar-chart'
  },
  Dự_báo_khai_thác:{
    name: 'Dự báo khai thác',
    path: 'du-bao-khai-thac',
    icon: 'nav-icon fas fa-question'
  },
  Người_dùng: {
    name: 'Người dùng',
    path: 'user',
    icon: 'nav-icon fas fa-users'
  },
  Phân_quyền: {
    name: 'Phân quyền',
    path: 'phan-quyen',
    icon: 'nav-icon fas fa-user-shield'
  }
}
