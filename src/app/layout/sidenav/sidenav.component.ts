import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { NguoiDungService } from '../../core/services/nguoi-dung.service';
import { sidemenu } from './data';
declare var $: any;

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  user: any;
  sidemenu :any
  constructor(
    private authService: AuthenticationService
  ) {
    this.sidemenu = sidemenu
  }

  ngOnInit() {
    //$('[data-widget="treeview"]').Treeview('init');
    this.user = this.authService.userValue
  }

  ListSide = [
    {icon: 'nav-icon fas fa-server', title: 'Thông tin hệ thống', path: 'thong-ke'},
    {icon: 'nav-icon fas fa-list', title: 'Quản lý tài nguyên', path: 'danh-muc'},
    {icon: 'nav-icon fas fa-database', title: 'Truy vấn dữ liệu', path: 'truy-van-data'},
    {icon: 'nav-icon fas fa-eye', title: 'Trực quan hóa dữ liệu', path: 'truc-quan-hoa'},
    {icon: 'nav-icon fas fa-gears', title: 'Chuẩn hóa dữ liệu', path: 'chuan-hoa-du-lieu'},
    {icon: 'nav-icon fas fa-question', title: 'Dự báo khai thác', path: 'du-bao-khai-thac'},
    {icon: 'nav-icon fas fa-users', title: 'Người dùng', path: 'user'},
    {icon: 'nav-icon fas fa-user-shield', title: 'Phân quyền', path: 'phan-quyen'},
  ]
}
