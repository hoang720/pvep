import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionPipeComponent } from './action-pipe.component';

describe('ActionPipeComponent', () => {
  let component: ActionPipeComponent;
  let fixture: ComponentFixture<ActionPipeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionPipeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionPipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
