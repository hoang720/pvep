import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChuanHoaDataService } from 'src/app/core/services/chuan-hoa-data.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-action-pipe',
  templateUrl: './action-pipe.component.html',
  styleUrls: ['./action-pipe.component.scss']
})
export class ActionPipeComponent implements OnInit {
  actionList: any;
  contentList: any = [];
  action: any = [];
  pipeDetail: any;
  param: any = []
  actionConfig: boolean = false
  valueEdit: string = '';
  pipeName: any

  constructor(
    private chuanHoaService: ChuanHoaDataService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // this.getActionPipe()
    this.route.params.subscribe(param => {
      this.pipeName = param.name
      this.getByname()
    })
  }

  //pipe
  ActionList = [
    {
      "name": "DataSource",
      "display_name": "Chọn nguồn dữ liệu",
      "params": [
        {
          "display_name": "Tên giếng",
          "name": "src",
          "value": "Well_A.xlsx",
          "type": "string"
        },
        {
          "name": "save",
          "display_name": "Lưu lịch sử",
          "value": true,
          "type": "bool"
        }
      ]
    },
    {
      "name": "MissingTransform",
      "display_name": "Xử lý Missing",
      "params": [
        {
          "name": "save",
          "display_name": "Lưu lịch sử",
          "value": true,
          "type": "bool"
        }
      ]
    },
    {
      "name": "RollingTimeWindow",
      "display_name": "RollingTimeWindow",
      "params": [
        {
          "name": "freq",
          "display_name": "Cửa sổ trượt",
          "value": 10,
          "type": "int"
        },
        {
          "name": "Q1",
          "display_name": "Q1",
          "value": 0.2,
          "type": "float"
        },
        {
          "name": "Q2",
          "display_name": "Q2",
          "value": 0.5,
          "type": "float"
        },
        {
          "name": "Q3",
          "display_name": "Q3",
          "value": 0.95,
          "type": "float"
        },
        {
          "name": "save",
          "display_name": "Lưu lịch sử",
          "value": true,
          "type": "bool"
        }
      ]
    },
    {
      "name": "DataDestination",
      "display_name": "Lưu dữ liệu"
    }
  ]
  getByname(){
    this.chuanHoaService.getBYname(this.pipeName).subscribe(data => {
      this.contentList = data.pipe
    })
  }
  getActionPipe(){
    this.chuanHoaService.getPipe().subscribe(data => {
      this.actionList = data[0].pipe
    })
  }
  addPipe(item: any){
    let item_copy = {...item}
    this.contentList.push(item_copy)
  }
  showDetail(action:any){
    this.action = action
    this.actionConfig = true
    this.param = action.params
  }
  closePipe(action: any){
    this.action = action
    this.actionConfig = false
  }
  drop(event: CdkDragDrop<any>) {
    moveItemInArray(this.contentList, event.previousIndex, event.currentIndex);
  }
  save(){
    this.chuanHoaService.addPipe(this.contentList, this.pipeName).subscribe(()=>{
      Swal.fire({
        title: 'Lưu thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.router.navigate([`/admin/chuan-hoa-du-lieu/`])
    })
  }
}
