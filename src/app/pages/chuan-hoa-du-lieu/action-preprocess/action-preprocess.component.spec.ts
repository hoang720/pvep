import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionPreprocessComponent } from './action-preprocess.component';

describe('ActionPreprocessComponent', () => {
  let component: ActionPreprocessComponent;
  let fixture: ComponentFixture<ActionPreprocessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionPreprocessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionPreprocessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
