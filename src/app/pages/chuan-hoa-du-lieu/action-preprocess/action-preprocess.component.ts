import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChuanHoaDataService } from 'src/app/core/services/chuan-hoa-data.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-action-preprocess',
  templateUrl: './action-preprocess.component.html',
  styleUrls: ['./action-preprocess.component.scss']
})
export class ActionPreprocessComponent implements OnInit {
  actionConfig: boolean = false
  contentList: any = []

  constructor(
    private chuanHoaService: ChuanHoaDataService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.pipeName = param.name
      this.getByName()
    })
  }

  pipeList = [
    {
      "name": "HDFSDataSource",
      "display_name": "Chọn nguồn dữ liệu",
      "params": [
        {
          "display_name": "Tên thư mục",
          "name": "source",
          "value": "PVEP/WellLog/PVEP",
          "type": "string"
        }
      ]
    },
    {
      "name": "HistoryProductionExtractor",
      "display_name": "Dữ liệu lịch sử khai thác từ Excel PVEP"
    },
    {
      "name": "WellLogPVEPExtractor",
      "display_name": "Trích rút WELL LOGS từ Excel PVEP"
    },
    {
      "name": "HbaseStorage",
      "display_name": "HbaseStorage",
      "params": [
        {
          "display_name": "Tên bảng",
          "name": "table_name",
          "value": "WELL_LOG",
          "type": "string"
        }
      ]
    }
  ]
  pipeName: any;
  Action: any = [];
  param: any = [];
  addPipe(item: any){
    let item_copy = {...item}
    this.contentList.push(item_copy)
  }
  getByName(){
    this.chuanHoaService.getPreBYname(this.pipeName).subscribe(data => {
      this.contentList = data.pipe
    })
  }
  save(){
    this.chuanHoaService.addPreprocess(this.contentList, this.pipeName).subscribe(() => {
      Swal.fire({
        title: 'thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.router.navigate([`/admin/chuan-hoa-du-lieu/`])
    })
  }
  drop(event: CdkDragDrop<any>) {
    moveItemInArray(this.contentList, event.previousIndex, event.currentIndex);
  }
  showDetail(action: any){
    this.Action = action
    this.actionConfig = true
    this.param = action.params
  }
  closePre(action: any){
    this.Action = action
    this.actionConfig = false
  }
}
