import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChuanHoaDataService } from 'src/app/core/services/chuan-hoa-data.service';

@Component({
  selector: 'app-chart-dialog',
  templateUrl: './chart-dialog.component.html',
  styleUrls: ['./chart-dialog.component.scss']
})
export class ChartDialogComponent implements OnInit {
  chartData: any;
  chartData1: any;
  chartData2: any;

  chartName: any
  constructor(
    private chuanHoaService: ChuanHoaDataService,
    public dialogRef: MatDialogRef<ChartDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA)
    public data: any,
  ) {
    this.chartName = data.data.name
  }

  chartOption: any;

  ngOnInit(): void {
    this.chart(this.chartName)
  }

  chart(name: any){
    this.chuanHoaService.chart(name).subscribe(data => {
      this.chartData = []
      for (var i in data) {

        const curChartData = []
        const tDay = []

        for (var j in data[i].data) {
          curChartData.push(data[i].data[j]["Q(BOPD)"].toFixed(3))
          tDay.push(Number(j))
        }

        const chartOption = {
          series: [
            {
              name: data[i].pipe_block,
              data: curChartData,
            }
          ],
          chart: {
            height: 350,
            type: "line",
            zoom: {
              enabled: true
            }
          },
          dataLabels: {
            enabled: false
          },
          stroke: {
            curve: "straight",
            colors: ["rgb(35, 241, 97)"],
            width: 1
          },
          title: {
            text: data[i].pipe_block,
            align: "left"
          },
          grid: {
            row: {
              colors: ["#f3f3f3", "transparent"],
              opacity: 0.5
            }
          },
          xaxis: {
            title: {
              text: 'T(DAY)',
            },
            type: 'numeric',
            categories: tDay
          },
          yaxis: {
            title: {
              text: "Q(BOPD)",
            },
          }
        }
        this.chartData.push(chartOption)

        if (data[i]['pipe_block'] == "RollingTimeWindow") {
          this.chartData1 = chartOption
        }
      }
    })
  }

  closeDialog(){
    this.dialogRef.close({ event:'Hủy Bỏ' })
  }
}
