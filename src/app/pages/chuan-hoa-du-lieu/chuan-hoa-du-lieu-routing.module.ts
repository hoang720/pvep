import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActionPipeComponent } from './action-pipe/action-pipe.component';
import { ActionPreprocessComponent } from './action-preprocess/action-preprocess.component';
import { ChuanHoaDuLieuComponent } from './chuan-hoa-du-lieu.component';

const routes: Routes = [
  { path: '', component: ChuanHoaDuLieuComponent },
  { path: 'action-pipe', component: ActionPipeComponent},
  { path: 'action-pipe/:name', component: ActionPipeComponent},
  { path: 'action-preprocess', component: ActionPreprocessComponent},
  { path: 'action-preprocess/:name', component: ActionPreprocessComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChuanHoaDuLieuRoutingModule { }
