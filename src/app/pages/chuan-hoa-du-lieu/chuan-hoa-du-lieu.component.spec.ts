import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChuanHoaDuLieuComponent } from './chuan-hoa-du-lieu.component';

describe('ChuanHoaDuLieuComponent', () => {
  let component: ChuanHoaDuLieuComponent;
  let fixture: ComponentFixture<ChuanHoaDuLieuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChuanHoaDuLieuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChuanHoaDuLieuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
