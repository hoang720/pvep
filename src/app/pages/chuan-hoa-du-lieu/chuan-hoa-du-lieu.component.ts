import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ChuanHoaDataService } from 'src/app/core/services/chuan-hoa-data.service';
import Swal from 'sweetalert2';
import { ChartDialogComponent } from './chart-dialog/chart-dialog.component';

@Component({
  selector: 'app-chuan-hoa-du-lieu',
  templateUrl: './chuan-hoa-du-lieu.component.html',
  styleUrls: ['./chuan-hoa-du-lieu.component.scss']
})
export class ChuanHoaDuLieuComponent implements OnInit {
  pipeList: any;
  selectedPipeName: any;
  loaded: boolean = false
  constructor(
    private chuanHoaService: ChuanHoaDataService,
    private router: Router,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getPipe()
    this.getPre()
  }

  //chuan-hoa-du-lieu
  getPipe(){
    this.chuanHoaService.getPipe().subscribe(data => {
      this.pipeList = data
    })
  }
  search(event: any){
    const filterValue = (event.target as HTMLInputElement).value;
    this.pipeList.filter = filterValue.trim().toLowerCase();
  }
  openToPage(){
    this.router.navigate([`/admin/chuan-hoa-du-lieu/action-pipe/`])
  }
  redrictPage(name: any){
    this.router.navigate([`/admin/chuan-hoa-du-lieu/action-pipe/${name}`])
  }
  runPipe(name: any){
    this.chuanHoaService.runPipe(name).subscribe(res => {
      Swal.fire({
        title: 'Thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.getPipe()
    })
  }
  openDialog(action: any, data: any){
    const dialogRef = this.dialog.open(ChartDialogComponent, {
      width: '900px',
      height: '800px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result)
      return
      if(result.action == 'View'){
        this.chartDialog(this.selectedPipeName)
      }
    })
  }
  chartDialog(name: any){
    this.chuanHoaService.chart(name).subscribe(data => {
      console.log(data)
    })
  }
  edit(name: any){
    Swal.fire({
      title: 'Bạn chắc chắn muốn xóa ?',
      text: "Thông tin sẽ bị xóa và không thể quay trở lại !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Xóa',
      cancelButtonText:'Hủy'
    }).then((result) => {
      if(result.isConfirmed){
        this.chuanHoaService.deletePipe(name).subscribe(()=>{
          Swal.fire({
            title: 'Xóa thành công!',
            icon: 'success',
            timer: 3000,
            showConfirmButton: true,
          });
          this.getPipe()
        })
      }
    })
  }


  //trich xuat du lieu
  preprocessList: any
  getPre(){
    this.chuanHoaService.getPreprocess().subscribe(data => {
      this.preprocessList = data
    })
  }
  addMore(){
    this.router.navigate([`/admin/chuan-hoa-du-lieu/action-preprocess/`])
  }
  redrictProcessPage(name: any){
    this.router.navigate([`/admin/chuan-hoa-du-lieu/action-preprocess/${name}`])
  }
  runData(name: any){
    this.chuanHoaService.runPreprocess(name).subscribe(result => {
      Swal.fire({
        title: 'Thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.getPre()
    })
  }
  deletePre(name: any){
    Swal.fire({
      title: 'Bạn chắc chắn muốn xóa ?',
      text: "Thông tin sẽ bị xóa và không thể quay trở lại !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Xóa',
      cancelButtonText:'Hủy'
    }).then((res) => {
      if(res.isConfirmed){
        this.chuanHoaService.deletePreprocess(name).subscribe(() => {
          Swal.fire({
            title: 'Xóa thành công!',
            icon: 'success',
            timer: 3000,
            showConfirmButton: true,
          });
          this.getPre()
        })
      }
    })
  }
}
