import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChuanHoaDuLieuRoutingModule } from './chuan-hoa-du-lieu-routing.module';
import { ChuanHoaDuLieuComponent } from './chuan-hoa-du-lieu.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from "@angular/material/input";
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ActionPipeComponent } from './action-pipe/action-pipe.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ChartDialogComponent } from './chart-dialog/chart-dialog.component';
import { NgApexchartsModule } from "ng-apexcharts";
import { MatDialogModule } from '@angular/material/dialog';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { MatTabsModule } from '@angular/material/tabs';
import { ActionPreprocessComponent } from './action-preprocess/action-preprocess.component';

@NgModule({
  declarations: [
    ChuanHoaDuLieuComponent,
    ActionPipeComponent,
    ChartDialogComponent,
    ActionPreprocessComponent
  ],
  imports: [
    CommonModule,
    ChuanHoaDuLieuRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    DragDropModule,
    MatAutocompleteModule,
    NgApexchartsModule,
    MatDialogModule,
    Ng2SearchPipeModule ,
    MatTabsModule
  ]
})
export class ChuanHoaDuLieuModule { }
