import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { FolderService } from '../../core/services/folder.service';
import { UploadDialogComponent } from './file-detail/upload-dialog/upload-dialog.component';
import { AddDialogComponent } from './file-detail/add-dialog/add-dialog.component';
import Swal from 'sweetalert2';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { createHostListener } from '@angular/compiler/src/core';

@Component({
  selector: 'app-danh-muc',
  templateUrl: './danh-muc.component.html',
  styleUrls: ['./danh-muc.component.scss']
})
export class DanhMucComponent implements OnInit {
  @Input('index') index: any;
  @Input('item') item: any;
  @ViewChild("tree")
  public tree!: TreeViewComponent;

  panelOpenState = false;
  folder_list: any;
  folder_sub_list: any;
  field!: Object;
  currentFolder: any;
  fullpath: any;

  constructor(
    private router: Router,
    private folderService: FolderService,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
    if(this.fullpath == null){
      this.folder_list = this.GetAllFolder()
    }else{
      this.folder_list = this.GetAllSUbFolder(this.fullpath)
    }
  }

  GetAllFolder() {
    this.folderService.getAll().subscribe(data => {
      this.folder_list = data
      this.currentFolder = data[0]
      this.fullpath = (this.currentFolder.path == '' ? '/' : this.currentFolder.path ) + this.currentFolder.name + '/'
      this.GetAllSUbFolder(this.fullpath)
      this.field = {
        dataSource: this.folder_list,
        id: '_id',
        text: 'name',
        child: 'children',
      };
    })
  }

  GetAllSUbFolder(path: any){
    this.folderService.getAllSUbFolder(path).subscribe(data => {
      this.folder_sub_list = data
    })
  }

  moreFolder(item: any){
    this.currentFolder = item
    this.fullpath = this.currentFolder.path == '' ? '/' : this.currentFolder.path  + this.currentFolder.name + '/'
    this.GetAllSUbFolder(this.fullpath)
  }

  UploadDialog(action: any, data: any): void{
    const dialogRef = this.dialog.open(UploadDialogComponent, {
      width: '500px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(!result)
        return
      else if (result.action == 'Upload'){
        let upload_data = {
          "path": this.currentFolder.path == '' ? '' : this.currentFolder.path  + this.currentFolder.name + '/',
          "file": result.data.file,
        }
        this.folderService.uploadFolder(upload_data.file, upload_data.path).subscribe(file_data => {
          upload_data = file_data
          Swal.fire({
            title: 'Tải lên file thành công!',
            icon: 'success',
            timer: 3000,
            showConfirmButton: true,
          });
          if(this.fullpath == null){
            this.folder_list = this.GetAllFolder()
          }else{
            this.folder_list = this.GetAllSUbFolder(this.fullpath)
          }
        })
        console.log(upload_data)
      }
    })
  }

  convertByte(x: any){
    const units = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    let l = 0, n = parseInt(x, 10) || 0;
    while(n >= 1024 && ++l){
        n = n/1024;
    }
    return(n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + units[l]);
  }

  downloadFile(path: any){
    this.folderService.getAllSUbFolder(path).subscribe(response => {
      window.open(response[0].link)
    })
  }

  openDialog(action: any, data: any){
    const dialogRef = this.dialog.open(AddDialogComponent, {
      width: '500px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(!result)
        return
      if (result.action == 'Thêm'){
        let add_data = {
          "path": result.data.path,
          "name": result.data.name,
          "type": 0
        }
        this.addFolderData(add_data)
      }
      else if (result.action == "Cập Nhật"){
        let update_data = {
          "name": result.data.name,
          "path": result.data.path,
        }
        this.updateFolderData(update_data, data._id)
      }
    })
  }

  addFolderData(folder: any){
    this.folderService.addFolder(folder).subscribe(data =>{
      Swal.fire({
        title: 'Thêm thư mục thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      if(this.fullpath == null){
        this.folder_list = this.GetAllFolder()
      }else{
        this.folder_list = this.GetAllSUbFolder(this.fullpath)
      }
    })
  }

  updateFolderData(folder: any, id: string){
    this.folderService.updateFolder(folder, id).subscribe(data =>{
      Swal.fire({
        title: 'Cập Nhật thư mục thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      if(this.fullpath == null){
        this.folder_list = this.GetAllFolder()
      }else{
        this.folder_list = this.GetAllSUbFolder(this.fullpath)
      }
    })
  }

  deleteFolder(id: string){
    Swal.fire({
      title: 'Bạn chắc chắn muốn xóa ?',
      text: "Thư mục sẽ bị xóa và không thể quay trở lại !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Xóa',
      cancelButtonText:'Hủy'
    }).then((result)=>{
      if(result.isConfirmed){
        this.folderService.deleteFolder(id).subscribe((data) => {
          Swal.fire(
            'Thành công!',
            'Thư mục đã được xóa',
            'success'
          )
          if(this.fullpath == null){
            this.folder_list = this.GetAllFolder()
          }else{
            this.folder_list = this.GetAllSUbFolder(this.fullpath)
          }
        })
      }
    })
  }
  public parentNodes: any = []

  nodeSelected(args: any) {
    this.parentNodes.push(args.node)
    this.findParentNodes(args.node);
    this.fullpath = '/';
    for (let i = this.parentNodes.length - 1; i >= 0; --i) {
      var data = this.tree.getNode(this.parentNodes[i]);
      this.fullpath = this.fullpath + data.text + '/';
      if (i == 0)
      this.fullpath = this.fullpath;
    }
    this.parentNodes = [];
    this.GetAllSUbFolder(this.fullpath)
  }
  public findParentNodes(node: HTMLElement) {
    let parent: any = node.parentElement?.parentElement;
    if (parent.classList.contains("e-list-item")) {
      this.parentNodes.push(parent);
      this.findParentNodes(parent);
    }
  }
}
