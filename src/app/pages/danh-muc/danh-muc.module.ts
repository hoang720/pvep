import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DanhMucRoutingModule } from './danh-muc-routing.module';
import { DanhMucComponent } from './danh-muc.component';
import { FileDetailComponent } from './file-detail/file-detail.component';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { AddDialogComponent } from './file-detail/add-dialog/add-dialog.component';
import { UploadDialogComponent } from './file-detail/upload-dialog/upload-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from "@angular/material/input";
import { MatExpansionModule } from '@angular/material/expansion';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { TreeViewModule } from "@syncfusion/ej2-angular-navigations";
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    DanhMucComponent,
    AddDialogComponent,
    UploadDialogComponent,
    FileDetailComponent,
  ],
  imports: [
    CommonModule,
    DanhMucRoutingModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule, ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatAutocompleteModule,
    TreeViewModule,
    NgxPaginationModule
  ]
})
export class DanhMucModule { }
