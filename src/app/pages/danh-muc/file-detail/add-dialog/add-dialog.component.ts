import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss']
})
export class AddDialogComponent implements OnInit {
  action: string;
  folderformdata: any;
  fg: FormGroup

  constructor(
    public dialogRef: MatDialogRef<AddDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA)
    public data: any,
    fb: FormBuilder
  ) {
    this.action = this.data.action;
    this.folderformdata = this.data.data;
    this.fg = fb.group({
      path: new FormControl( this.folderformdata.path ),
      name: new FormControl( this.folderformdata.name, [Validators.required]),
      type: new FormControl(0)
    })
  }

  ngOnInit(): void {
  }


  actionDialog(){
    this.dialogRef.close({
      action: this.action,
      data: this.fg.value
    })
  }

  closeDialog(){
    this.dialogRef.close({ event:'Hủy Bỏ' })
  }
}
