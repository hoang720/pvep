import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FolderService } from '../../../core/services/folder.service';
import { UploadDialogComponent } from '../file-detail/upload-dialog/upload-dialog.component';
import { AddDialogComponent } from '../file-detail/add-dialog/add-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-file-detail',
  templateUrl: './file-detail.component.html',
  styleUrls: ['./file-detail.component.scss']
})
export class FileDetailComponent implements OnInit {

  data_global: any;
  folder_name: any;

  dataSource = new MatTableDataSource<any>();
  @ViewChild('paginator') paginator: any;
  constructor(
    private folderService: FolderService,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
  ) {
    this.data_global = JSON.parse(localStorage.getItem('DATA_GLOBAL')!);
    this.folder_name = this.route.snapshot.paramMap.get('folder_name')
  }

  ngOnInit(): void {
    this.getByName(this.folder_name)
  }
  displayedColumns: string[] = [
    'FileName',
    'CreatedTime',
    'Type',
  ]
  openFile(){
    
  }
  getByName(folder_name: any){
    this.folderService.getFolderByName(folder_name).subscribe(data => {
      this.dataSource.data = data
    })
  }

  openUpDialog(action: any, data: any): void{
    const dialogRef = this.dialog.open(UploadDialogComponent, {
      width: '500px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(!result) 
        return
      else if (result.action == 'Upload'){

      }
    })
  }
  openDialog(action: any, data: any){
    const dialogRef = this.dialog.open(AddDialogComponent, {
      width: '500px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(!result) 
        return
      if (result.action == 'Thêm'){
        var add_data = {
          "path": result.data.path,
          "name": result.data.name,
          "type": result.data.type
        }
        this.addFolderData(add_data)
      }
      else if (result.action == "Cập Nhật"){
        var update_data = {
          "path": result.data.path,
          "name": result.data.name,
        }
        this.updateFolderData(update_data, data._id)
      }
    })
  }

  addFolderData(folder: any){
    this.folderService.addFolder(folder).subscribe(data =>{
      Swal.fire({
        title: 'Thêm thư mục thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.getByName(this.folder_name)
      console.log(data);
    })
  }

  updateFolderData(folder: any, id: string){
    this.folderService.updateFolder(folder, id).subscribe(data =>{
      Swal.fire({
        title: 'Cập Nhật thư mục thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.getByName(this.folder_name)
      console.log(data);
    })
  }

  deleteFolder(id: string){
    Swal.fire({
      title: 'Bạn chắc chắn muốn xóa ?',
      text: "Thư mục sẽ bị xóa và không thể quay trở lại !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Xóa',
      cancelButtonText:'Hủy'
    }).then((result)=>{
      if(result.isConfirmed){
        this.folderService.deleteFolder(id).subscribe((data) => {
          Swal.fire(
            'Thành công!',
            'Thông tin đã được xóa',
            'success'
          )
          this.getByName(this.folder_name)
        })
      }
    })
  }
}
