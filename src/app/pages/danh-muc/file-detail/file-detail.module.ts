import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FileDetailRoutingModule } from './file-detail-routing.module';
import { FileDetailComponent } from './file-detail.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from '@angular/material/table';

@NgModule({
    declarations: [
        /* FileDetailComponent */
    ],
    imports: [
        CommonModule,
        FileDetailRoutingModule,
        FormsModule, ReactiveFormsModule,
        MatTableModule
    ]
})

export class FileDetailModule {}