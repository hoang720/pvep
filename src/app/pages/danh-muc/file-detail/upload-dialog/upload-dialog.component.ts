import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.scss']
})
export class UploadDialogComponent implements OnInit {
  action: string;
  fg: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<UploadDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any, fb: FormBuilder
  ) {
    this.action = this.data.action;
    this.fg = fb.group({
      file: [null]
    })
  }

  ngOnInit(): void {}

  uploadFile(event: any){
    const file = event.target.files[0];
    this.fg.patchValue({
      file: file,
    });
    this.fg.get('file')
  }
  doAction(){
    this.dialogRef.close({
      action: this.action, 
      data: this.fg.value
    })
  }

  closeDialog(){
    this.dialogRef.close({ event:'Hủy Bỏ' })
  }
}
