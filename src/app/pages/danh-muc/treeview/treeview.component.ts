import { Component, Inject, ViewChild,  OnInit } from "@angular/core";
import {
  TreeViewComponent,
  NodeClickEventArgs,
  BeforeOpenCloseMenuEventArgs,
  MenuEventArgs,
  MenuItemModel,
  ContextMenuComponent,
  NodeEditEventArgs
} from "@syncfusion/ej2-angular-navigations";


@Component({
  selector: 'app-treeview',
  templateUrl: './treeview.component.html',
  styleUrls: ['./treeview.component.scss']
})
export class TreeviewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  /* folderMode: any;

  public hierarchicalData: Object[] = [
    {
      id: "01",
      name: "Local Disk (C:)",
      expanded: true,
      eimg: "10",
      hasAttribute: { class: "remove rename" },
      subChild: [
        {
          id: "01-01",
          name: "Program Files",
          eimg: "2",
          subChild: [
            { id: "01-01-01", name: "Windows NT", eimg: "1" },
            { id: "01-01-02", name: "Windows Mail", eimg: "4" },
            { id: "01-01-03", name: "Windows Photo Viewer", eimg: "9" }
          ]
        },
        {
          id: "01-02",
          name: "Users",
          expanded: true,
          eimg: "8",
          subChild: [
            { id: "01-02-01", name: "Smith", eimg: "6" },
            { id: "01-02-02", name: "Public", eimg: "3" },
            { id: "01-02-03", name: "Admin", eimg: "1" }
          ]
        },
        {
          id: "01-03",
          name: "Windows",
          eimg: "6",
          subChild: [
            { id: "01-03-01", name: "Boot", eimg: "1" },
            { id: "01-03-02", name: "FileManager", eimg: "4" },
            { id: "01-03-03", name: "System32", eimg: "9" }
          ]
        }
      ]
    },
    {
      id: "02",
      name: "Local Disk (D:)",
      eimg: "4",
      hasAttribute: { class: "remove" },
      subChild: [
        {
          id: "02-01",
          name: "Personals",
          eimg: "6",
          subChild: [
            { id: "02-01-01", name: "My photo.png", eimg: "3" },
            { id: "02-01-02", name: "Rental document.docx", eimg: "1" },
            { id: "02-01-03", name: "Pay slip.pdf", eimg: "9" }
          ]
        },
        {
          id: "02-02",
          name: "Projects",
          eimg: "9",
          subChild: [
            { id: "02-02-01", name: "ASP Application", eimg: "4" },
            { id: "02-02-02", name: "TypeScript Application", eimg: "6" },
            { id: "02-02-03", name: "React Application", eimg: "3" }
          ]
        }
      ]
    }
  ];
  public field: Object = {
    dataSource: this.hierarchicalData,
    id: "id",
    text: "name",
    child: "subChild",
    htmlAttributes: "hasAttribute"
  };

  @ViewChild("wsTree") wsTree!: TreeViewComponent;
  @ViewChild("contentmenutree") contentmenutree!: ContextMenuComponent;

  createNodeId!: string;

  public nodeclicked(args: NodeClickEventArgs) {
    if (args.event.which === 3) {
      this.wsTree.selectedNodes = [args.node.getAttribute("data-uid")];
    }
  }

  public onNodeEdited(args: NodeEditEventArgs): void {
    if (args.newText.trim() == "") {
      var obj = this.wsTree.getTreeData(this.wsTree.selectedNodes[0]);
      console.log(obj);
      setTimeout(() => {
        this.wsTree.removeNodes([args.node]);
      }, 2);
    } else if (args.newText != args.oldText) {
      var obj = this.wsTree.getTreeData(this.wsTree.selectedNodes[0]);
      if (obj[0].hasOwnProperty("resourceType")) {
        let element = this.wsTree.element.querySelector(
          '[data-uid="' + args.nodeData.id + '"]'
        );
        var nodeData: any = this.wsTree.getTreeData(args.nodeData.id as any);
      } else {
        let element = this.wsTree.element.querySelector(
          '[data-uid="' + args.nodeData.id + '"]'
        );
        element.setAttribute("data-uid", "12345678");
        var nodeData: any = this.wsTree.getTreeData(args.nodeData.id as any);
        nodeData[0].id = "12345678";
        console.log(nodeData);
        console.log(this.wsTree.getTreeData());
      }
    }
  }

  public menuItems: MenuItemModel[] = [
    { text: "Add Folder" },
    { text: "Rename Folder" },
    { text: "Copy Folder" },
    { text: "Move Folder" },
    { text: "Delete Folder" }
  ];

  public index: number = 1;

  public menuclick(args: MenuEventArgs) {
    let targetNodeId: string = this.wsTree.selectedNodes[0];
    if (args.item.text == "Add Folder") {
      //this.router.navigate(['folder'],{relativeTo: this.route});
      // this.addFolder_sb.show();
      // this.closeRightSidebar();
      this.folderMode = "create";
      let nodeId: string = "tree_" + new Date().getTime();
      this.createNodeId = nodeId;
      let item: { [key: string]: Object } = { id: nodeId, name: "", eimg: "9" };
      this.wsTree.addNodes([item], targetNodeId);
      this.wsTree.beginEdit(nodeId);
    } else if (args.item.text == "Delete Folder") {
      var obj = this.wsTree.getTreeData(this.wsTree.selectedNodes[0]);
      let element = this.wsTree.element.querySelector(
        '[data-uid="' + obj[0].id + '"]'
      );
      this.wsTree.removeNodes([element]);
    } else if (args.item.text == "Rename Folder") {
      this.folderMode = "rename";
      // setTimeout(() => {
      console.log(this.wsTree.selectedNodes[0]);
      var obj = this.wsTree.getTreeData(this.wsTree.selectedNodes[0]);
      console.log(obj);
      this.wsTree.beginEdit(obj[0].id);
      // }, 100);
    }
  }

  public beforeopen(args: BeforeOpenCloseMenuEventArgs) {
    console.log(this.wsTree.selectedNodes[0]);
    var nodeData = this.wsTree.getTreeData(this.wsTree.selectedNodes[0]);
    console.log(nodeData);
    if (nodeData[0].hasOwnProperty("resourceType")) {
      this.contentmenutree.hideItems([
        "Rename Folder",
        "Copy Folder",
        "Move Folder",
        "Delete Folder"
      ]);
    } else {
      this.contentmenutree.showItems([
        "Rename Folder",
        "Copy Folder",
        "Move Folder",
        "Delete Folder"
      ]);
    }
  } */
}
