import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataHinhAnhComponent } from './data-hinh-anh.component';

const routes: Routes = [{ path: '', component: DataHinhAnhComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataHinhAnhRoutingModule { }
