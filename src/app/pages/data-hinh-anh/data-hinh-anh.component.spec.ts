import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataHinhAnhComponent } from './data-hinh-anh.component';

describe('DataHinhAnhComponent', () => {
  let component: DataHinhAnhComponent;
  let fixture: ComponentFixture<DataHinhAnhComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataHinhAnhComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataHinhAnhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
