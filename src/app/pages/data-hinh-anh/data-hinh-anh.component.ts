import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { TrucQuanHoaService } from 'src/app/core/services/truc-quan-hoa.service';
import { ImageDetailComponent } from './image-detail/image-detail.component';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-data-hinh-anh',
  templateUrl: './data-hinh-anh.component.html',
  styleUrls: ['./data-hinh-anh.component.scss']
})
export class DataHinhAnhComponent implements OnInit {
  dataSource = new MatTableDataSource<any>();
  imageName: any;
  Total: any;
  page = 1;
  page_size = 45;
  field!: Object;
  @ViewChild('paginator') paginator: any;
  constructor(
    private ImageService: TrucQuanHoaService,
    public dialog: MatDialog,
  )
  { }

  displayedColumns: string[] = [
    'Name',
    'Image',
    'Type',
    'CreatedTime',/* 
    'download' */
  ]

  ngOnInit(): void {
    if(this.imageName == null){
      this.getAllImage()
    }else{
      this.getImageName(this.imageName)
    }
  }

  openDialog(action: any, data: any){
    const dialogRef = this.dialog.open(ImageDetailComponent, {
      width: '900px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result)
      return 
      if(result.action == 'View'){
        this.getImageName(this.imageName)
      }
    })
  }

  getAllImage(){
    this.ImageService.getImage(this.page, this.page_size).subscribe(data => {
      this.dataSource.data = data.data
      this.Total = data.total
      this.field = { 
        dataSource: this.dataSource.data, 
        id: '_id', 
        text: 'name', 
      };
    })
  }

  getImageName(imageName: any){
    this.ImageService.getImageName(imageName).subscribe(data => {
      this.dataSource.data = data.data
    })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  nodeSelected(args:any) {
    this.imageName = args.node.innerText;
    this.getImageName(this.imageName)
  }
}
