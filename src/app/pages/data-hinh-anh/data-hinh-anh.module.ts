import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataHinhAnhRoutingModule } from './data-hinh-anh-routing.module';
import { DataHinhAnhComponent } from './data-hinh-anh.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TreeViewModule } from "@syncfusion/ej2-angular-navigations";
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDialogModule } from '@angular/material/dialog';
import { ImageDetailComponent } from './image-detail/image-detail.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    DataHinhAnhComponent,
  ],
  imports: [
    CommonModule,
    DataHinhAnhRoutingModule,
    FormsModule, ReactiveFormsModule,
    TreeViewModule,
    MatTableModule,
    MatPaginatorModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule
  ]
})
export class DataHinhAnhModule { }
