import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-image-detail',
  templateUrl: './image-detail.component.html',
  styleUrls: ['./image-detail.component.scss']
})
export class ImageDetailComponent implements OnInit {

  imagedata: any;
  fg: FormGroup
  constructor(
    public dialogRef: MatDialogRef<ImageDetailComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA)
    public data: any,
    fb: FormBuilder
  ) {
    this.imagedata = this.data.data
    this.fg = fb.group({
      link : new FormControl ( this.imagedata.link )
    })
  }

  ngOnInit(): void {

  }

  closeDialog(){
    this.dialogRef.close({ event:'Hủy Bỏ' })
  }
}
