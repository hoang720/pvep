import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataKhaiThacComponent } from './data-khai-thac.component';

const routes: Routes = [{ path: '', component: DataKhaiThacComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataKhaiThacRoutingModule { }
