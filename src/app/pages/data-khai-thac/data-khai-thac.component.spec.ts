import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataKhaiThacComponent } from './data-khai-thac.component';

describe('DataKhaiThacComponent', () => {
  let component: DataKhaiThacComponent;
  let fixture: ComponentFixture<DataKhaiThacComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataKhaiThacComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataKhaiThacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
