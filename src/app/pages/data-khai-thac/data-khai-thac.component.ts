import { Component, OnInit } from '@angular/core';
import { TrucQuanHoaService } from 'src/app/core/services/truc-quan-hoa.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-data-khai-thac',
  templateUrl: './data-khai-thac.component.html',
  styleUrls: ['./data-khai-thac.component.scss']
})
export class DataKhaiThacComponent implements OnInit {
  welllogSelected = 'Well_A.xlsx'
  welllogsList: any;
  explodeDataTDAY: any;
  explodeDataPSIA: any;
  explodeDataQSTB: any;
  linechart1: any;
  linechart2: any;
  scatterchart: any;
  constructor(
    private ExplodeDataService: TrucQuanHoaService
  ) {
   
  }

  ngOnInit(): void {
    this.getWelllogsList()
    this.getDataList()
  }

  getDataList(){
    this.ExplodeDataService.getData(this.welllogSelected).subscribe(data => {
      this.explodeDataTDAY = data["T(DAY)"]
      this.explodeDataPSIA = data["P(PSIA)"]
      this.explodeDataQSTB = data["Q(STB/D)"]
      this.linechart1 = {
        series: [
          { 
            name: "P(PSIA)",
            data:  this.explodeDataPSIA
          }
        ],
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: true
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(18, 18, 243)"],
          width: 1
        },
        title: {
          text: "",
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        yaxis: {
          title: {
            text: 'P(PSIA)',
          },
        },
        xaxis: {
          title: {
            text: 'T(DAY)',
          },
          type: 'numeric',
          categories: this.explodeDataTDAY
        }
      }
      this.linechart2 = {
        series: [
          { 
            name: "Q(STB/D)",
            data: this.explodeDataQSTB
          }
        ],
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: true
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(245, 17, 17)"],
          width: 1
        },
        title: {
          text: "",
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        yaxis: {
          title: {
            text: 'Q(STB/D)',
          },
        },
        xaxis: {
          title: {
            text: 'T(DAY)',
          },
          type: 'numeric',
          categories: this.explodeDataTDAY
        }
      }

      this.scatterchart = {
        series: [
          {
            name: "P(PSIA)",
            data: this.explodeDataPSIA
          },{
            name: "Q(STB/D)",
            data: this.explodeDataQSTB
          }
        ],
        title: {
          text: "",
          align: "left"
        },
        chart: {
          animations: {
            enabled: false,
            speed: 800
          },
          height: 350,
          type: 'scatter',
          zoom: {
            enabled: true,
            type: 'xy'
          }
        },
        markers: {
          
          size: 4
        },
        xaxis: {
          type: 'numeric',
          title: {
            text: 'T(DAY)',
          },
          tickAmount: 10,
          categories: this.explodeDataTDAY
        },
        yaxis: {
          tickAmount: 7
        }
      }
      console.log(this.explodeDataTDAY)
    })
  
  }

  getWelllogsList(){
    this.ExplodeDataService.getListWellog('histprod_test').subscribe(data => {
      this.welllogsList = data
    })
  }
}
