import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataKhaiThacRoutingModule } from './data-khai-thac-routing.module';
import { DataKhaiThacComponent } from './data-khai-thac.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormsModule } from '@angular/forms';
import { NgApexchartsModule } from "ng-apexcharts";

@NgModule({
  declarations: [
    DataKhaiThacComponent
  ],
  imports: [
    CommonModule,
    DataKhaiThacRoutingModule,
    MatAutocompleteModule,
    FormsModule,
    NgApexchartsModule
  ]
})
export class DataKhaiThacModule { }
