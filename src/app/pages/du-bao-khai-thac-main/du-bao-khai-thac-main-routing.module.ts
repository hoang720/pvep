import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DuBaoKhaiThacMainComponent } from './du-bao-khai-thac-main.component';

const routes: Routes = [{ path: '', component: DuBaoKhaiThacMainComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DuBaoKhaiThacMainRoutingModule { }
