import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DuBaoKhaiThacMainComponent } from './du-bao-khai-thac-main.component';

describe('DuBaoKhaiThacMainComponent', () => {
  let component: DuBaoKhaiThacMainComponent;
  let fixture: ComponentFixture<DuBaoKhaiThacMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DuBaoKhaiThacMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DuBaoKhaiThacMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
