import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DuBaoKhaiThacMainRoutingModule } from './du-bao-khai-thac-main-routing.module';
import { DuBaoKhaiThacMainComponent } from './du-bao-khai-thac-main.component';


@NgModule({
  declarations: [
    DuBaoKhaiThacMainComponent
  ],
  imports: [
    CommonModule,
    DuBaoKhaiThacMainRoutingModule
  ]
})
export class DuBaoKhaiThacMainModule { }
