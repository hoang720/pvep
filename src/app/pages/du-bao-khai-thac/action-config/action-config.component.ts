import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DuBaoKhaiThacService } from 'src/app/core/services/du-bao-khai-thac.service';

@Component({
  selector: 'app-action-config',
  templateUrl: './action-config.component.html',
  styleUrls: ['./action-config.component.scss']
})
export class ActionConfigComponent implements OnInit {
  actionConfig: boolean = false;
  contentList: any = [];
  action: any = [];
  param: any = []
  modelName: any;
  constructor(
    private modelService: DuBaoKhaiThacService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.modelName = param.name
      this.getModelName()
    })
  }
  ActionList = [
    {
      "name": "PVEPMongoDataSource",
      "display_name": "Chọn nguồn dữ liệu",
      "params": [
        {
          "display_name": "Tiến trình",
          "name": "pipe_process_name",
          "value": "Giếng A",
          "type": "string"
        },
        {
          "display_name": "Khối dữ liệu",
          "name": "pipe_block",
          "value": "DataSource",
          "type": "string"
        }
      ]
    },
    {
      "name": "PVEPPrepareDatasetTransform",
      "display_name": "Chuẩn bị dữ liệu huấn luyện PVEP",
      "params": [
        {
          "display_name": "Tỉ lệ tập test",
          "name": "test_ratio",
          "value": 0.15,
          "type": "number"
        },
        {
          "display_name": "Tỉ lệ tập validation",
          "name": "valid_ratio",
          "value": 0.15,
          "type": "number"
        },
        {
          "display_name": "Chuẩn hóa",
          "name": "isNorm",
          "value": true,
          "type": "bool"
        },
        {
          "display_name": "Look back",
          "name": "look_back",
          "value": 20,
          "type": "int"
        },
      ]
    },
    {
      "name": "PVEP_RNN_HistoryProductionEstimator",
      "display_name": "RNN Prediction Model",
      "params": [
        {
          "display_name": "Huấn luyện lại",
          "name": "retrain",
          "value": true,
          "type": "bool"
        },
        {
          "display_name": "Epochs",
          "name": "epochs",
          "value": 10,
          "type": "int"
        },
        {
          "display_name": "Batch Size",
          "name": "batch_size",
          "value": 128,
          "type": "int"
        },
        {
          "display_name": "Thư mục lưu mô hình",
          "name": "checkpoint_dir",
          "value": 'model/pvep/checkpoint',
          "type": "string"
        }
      ]
    },
    {
      "name": "PVEP_GRU_HistoryProductionEstimator",
      "display_name": "GRU Prediction Model",
      "params": [
        {
          "display_name": "Huấn luyện lại",
          "name": "retrain",
          "value": true,
          "type": "bool"
        },
        {
          "display_name": "Epochs",
          "name": "epochs",
          "value": 10,
          "type": "int"
        },
        {
          "display_name": "Batch Size",
          "name": "batch_size",
          "value": 128,
          "type": "int"
        },
        {
          "display_name": "Thư mục lưu mô hình",
          "name": "checkpoint_dir",
          "value": 'model/pvep/checkpoint',
          "type": "string"
        }
      ]
    },
    {
      "name": "PVEP_ANN_HistoryProductionEstimator",
      "display_name": "ANN Prediction Model",
      "params": [
        {
          "display_name": "Huấn luyện lại",
          "name": "retrain",
          "value": true,
          "type": "bool"
        },
        {
          "display_name": "Epochs",
          "name": "epochs",
          "value": 10,
          "type": "int"
        },
        {
          "display_name": "Batch Size",
          "name": "batch_size",
          "value": 128,
          "type": "int"
        },
        {
          "display_name": "Thư mục lưu mô hình",
          "name": "checkpoint_dir",
          "value": 'model/pvep/checkpoint',
          "type": "string"
        }
      ]
    },
    {
      "name": "PVEP_LSTM_HistoryProductionEstimator",
      "display_name": "LSTM Prediction Model",
      "params": [
        {
          "display_name": "Huấn luyện lại",
          "name": "retrain",
          "value": true,
          "type": "bool"
        },
        {
          "display_name": "Epochs",
          "name": "epochs",
          "value": 10,
          "type": "int"
        },
        {
          "display_name": "Batch Size",
          "name": "batch_size",
          "value": 128,
          "type": "int"
        },
        {
          "display_name": "Thư mục lưu mô hình",
          "name": "checkpoint_dir",
          "value": 'model/pvep/checkpoint',
          "type": "string"
        }
      ]
    }
  ]
  close(action: any){
    this.actionConfig = false
    this.action = action
  }
  drop(event: CdkDragDrop<any>) {
    moveItemInArray(this.contentList, event.previousIndex, event.currentIndex);
  }
  showDetail(action:any){
    this.action = action
    this.actionConfig = true
    this.param = action.params
  }
  addModel(item: any){
    let item_copy = {...item}
    this.contentList.push(item_copy)
  }
  getModelName(){
    this.modelService.getModelName(this.modelName).subscribe(data => {
      this.contentList = data.pipe
    })
  }
  removeModel(){
    this.contentList.pop()
  }
  save(){
    this.modelService.addModel(this.contentList, this.modelName).subscribe(() => {
      Swal.fire({
        title: 'Lưu thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.router.navigate([`/admin/huan-luyen-mo-hinh/`])
    })
  }
}
