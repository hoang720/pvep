import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DuBaoKhaiThacService } from 'src/app/core/services/du-bao-khai-thac.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  chartData: any;
  chartData1: any;
  chartName: any;
  chartOption: any;
  constructor(
    private chartService: DuBaoKhaiThacService,
    public dialogRef: MatDialogRef<ChartComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA)
    public data: any,
  ) {
    this.chartName = data.data.name
  }

  ngOnInit(): void {
    this.chart(this.chartName)
  }
  chart(name: any){
    this.chartService.chartModel(name).subscribe(data => {
      this.chartData = []
      for (var i in data){
        for (var j in data[i].data){
          this.chartOption = {
            series: [
              {
                name: "loss",
                data: data[i].data["train_log"]["loss"],
              },
              {
                name: "mea",
                data: data[i].data["train_log"]["mae"],
              },
              {
                name: "val_loss",
                data: data[i].data["train_log"]["val_loss"],
              },
              {
                name: "val_mae",
                data: data[i].data["train_log"]["val_mae"],
              },
            ],

            chart: {
              height: 350,
              type: "line",
              zoom: {
                enabled: true
              }
            },
            dataLabels: {
              enabled: false
            },
            stroke: {
              curve: "straight",

              width: 1
            },
            title: {
              text: data[i].pipe_block,
              align: "left"
            },
            grid: {
              row: {
                colors: ["#f3f3f3", "transparent"],
                opacity: 0.5
              }
            },
            xaxis: {
              title: {
                text: '',
              },
              type: 'numeric',
              categories: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            },
            yaxis: {
              title: {
                text: "",
              },
            }
          }
        }
        console.log(data[i].data["train_log"])
        this.chartData.push(this.chartOption)
        if (data[i]['pipe_block'] == "PVEP_RNN_HistoryProductionEstimator") {
          this.chartData1 = this.chartOption
        }
      }
    })
  }
  closeDialog(){
    this.dialogRef.close({ event:'Hủy Bỏ' })
  }
}
