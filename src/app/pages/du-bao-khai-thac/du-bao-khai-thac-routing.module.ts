import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActionConfigComponent } from './action-config/action-config.component';
import { DuBaoKhaiThacComponent } from './du-bao-khai-thac.component';

const routes: Routes = [
  { path: '', component: DuBaoKhaiThacComponent },
  { path: 'action-model', component: ActionConfigComponent},
  { path: 'action-model/:name', component: ActionConfigComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DuBaoKhaiThacRoutingModule { }
