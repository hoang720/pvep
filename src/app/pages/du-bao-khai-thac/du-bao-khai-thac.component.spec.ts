import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DuBaoKhaiThacComponent } from './du-bao-khai-thac.component';

describe('DuBaoKhaiThacComponent', () => {
  let component: DuBaoKhaiThacComponent;
  let fixture: ComponentFixture<DuBaoKhaiThacComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DuBaoKhaiThacComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DuBaoKhaiThacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
