import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DuBaoKhaiThacService } from 'src/app/core/services/du-bao-khai-thac.service';
import { ChartComponent } from './chart/chart.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-du-bao-khai-thac',
  templateUrl: './du-bao-khai-thac.component.html',
  styleUrls: ['./du-bao-khai-thac.component.scss']
})
export class DuBaoKhaiThacComponent implements OnInit {
  modelName: any;
  modelList: any;
  loaded: boolean = false
  constructor(
    private modelService: DuBaoKhaiThacService,
    private router: Router,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getModel()
  }

  getModel(){
    this.modelService.getModel().subscribe(data => {
      this.modelList = data
    })
  }
  addModel(){
    this.router.navigate([`/admin/huan-luyen-mo-hinh/action-model/`])
  }
  openToDetail(name: any){
    this.router.navigate([`/admin/huan-luyen-mo-hinh/action-model/${name}`])
  }
  runModel(name: any){
    this.modelService.runModel(name).subscribe(res => {
      this.getModel()
    })
  }
  openDialog(action: any, data: any){
    const dialogRef = this.dialog.open(ChartComponent, {
      width: '900px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result)
      return
      if(result.action == 'View'){
        this.viewChartDialog(this.modelName)
      }
    })
  }
  viewChartDialog(name: any){
    this.modelService.chartModel(name).subscribe(data => {
      console.log(data)
    })
  }
  deleteModel(name: any){
    Swal.fire({
      title: 'Bạn chắc chắn muốn xóa ?',
      text: "Thông tin sẽ bị xóa và không thể quay trở lại !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Xóa',
      cancelButtonText:'Hủy'
    }).then((result) => {
      if(result.isConfirmed){
        this.modelService.deleteModel(name).subscribe(() => {
          Swal.fire({
            title: 'Xóa thành công!',
            icon: 'success',
            timer: 3000,
            showConfirmButton: true,
          });
          this.getModel()
        })
      }
    })
  }
}
