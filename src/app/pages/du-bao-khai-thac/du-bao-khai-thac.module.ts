import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DuBaoKhaiThacRoutingModule } from './du-bao-khai-thac-routing.module';
import { DuBaoKhaiThacComponent } from './du-bao-khai-thac.component';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ActionConfigComponent } from './action-config/action-config.component';
import { ChartComponent } from './chart/chart.component';
import { MatDialogModule } from '@angular/material/dialog';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgApexchartsModule } from "ng-apexcharts";

@NgModule({
  declarations: [
    DuBaoKhaiThacComponent,
    ActionConfigComponent,
    ChartComponent
  ],
  imports: [
    CommonModule,
    DuBaoKhaiThacRoutingModule,
    MatTabsModule,
    FormsModule,ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    Ng2SearchPipeModule,
    NgApexchartsModule
  ]
})
export class DuBaoKhaiThacModule { }
