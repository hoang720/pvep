import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionPrivilegeComponent } from './action-privilege.component';

describe('ActionPrivilegeComponent', () => {
  let component: ActionPrivilegeComponent;
  let fixture: ComponentFixture<ActionPrivilegeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionPrivilegeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionPrivilegeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
