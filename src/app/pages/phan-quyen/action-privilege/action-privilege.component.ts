import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PhanQuyenService } from 'src/app/core/services/phan-quyen.service';

@Component({
  selector: 'app-action-privilege',
  templateUrl: './action-privilege.component.html',
  styleUrls: ['./action-privilege.component.scss']
})
export class ActionPrivilegeComponent implements OnInit {
  contentList: any = [];
  status: boolean = true;
  desc: any;
  privilegeName: any;
  privilegeList =
  [
    "Thông_tin_hệ_thống",
    "Quản_lý_tài_nguyên",
    "Truy_vấn_dữ_liệu",
    "Trực_quan_hóa_dữ_liệu",
    "Chuẩn_hóa_dữ_liệu",
    "Huấn_luyện_mô_hình",
    "Dự_báo_khai_thác",
    "Người_dùng",
    "Phân_quyền"
  ]


  constructor(
    private QuyenService: PhanQuyenService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.privilegeName = param.name
      this.getRoleByName()
    })
  }

  drop(event: CdkDragDrop<any>) {
    moveItemInArray(this.contentList, event.previousIndex, event.currentIndex);
  }
  getRoleByName(){
    this.QuyenService.getQuyenByName(this.privilegeName).subscribe(data=>{
      this.contentList = data[0].privilege
    })
  }
  getAllRole(){
    this.QuyenService.getAllQuyen().subscribe(data=>{
      console.log(data)
    })
  }
  addPri(item: any){
    let item_copy = item
    this.contentList.push(item_copy)
  }
  removePri(){
    this.contentList.pop()
  }
  save(){
    this.QuyenService.addQuyen(this.privilegeName).subscribe(data=>{
      console.log(data)
    })
  }
}
