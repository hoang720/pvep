import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhanQuyenDialogComponent } from './phan-quyen-dialog.component';

describe('PhanQuyenDialogComponent', () => {
  let component: PhanQuyenDialogComponent;
  let fixture: ComponentFixture<PhanQuyenDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhanQuyenDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhanQuyenDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
