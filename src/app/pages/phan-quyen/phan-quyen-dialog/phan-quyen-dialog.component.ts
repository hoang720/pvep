import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { PhanQuyenService } from 'src/app/core/services/phan-quyen.service';
import { NguoiDungService } from 'src/app/core/services/nguoi-dung.service';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-phan-quyen-dialog',
  templateUrl: './phan-quyen-dialog.component.html',
  styleUrls: ['./phan-quyen-dialog.component.scss']
})
export class PhanQuyenDialogComponent implements OnInit {
  action: any;
  formdata: any;
  fg: FormGroup;
  statusList = [
    {id: 1, text: 'true'},
    {id: 2, text: 'false'}
  ]
  constructor(
    public dialogRef: MatDialogRef<PhanQuyenDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA)
    public data: any,
    private fb: FormBuilder,
    private quyenService: PhanQuyenService,
    private userService: NguoiDungService
  ) {
    this.action = this.data.action;
    this.formdata = this.data.data;

    this.fg = fb.group({
      name: new FormControl( this.formdata.name, [Validators.required]),
      mo_ta: new FormControl( this.formdata.mo_ta, [Validators.required]),
      status: new FormControl( this.formdata.status ),
      privilege: new FormArray([
        new FormControl( this.formdata.privilege )
      ]),
    })
  }

  privilegeList =
  [
    "Thông_tin_hệ_thống",
    "Quản_lý_tài_nguyên",
    "Truy_vấn_dữ_liệu",
    "Trực_quan_hóa_dữ_liệu",
    "Chuẩn_hóa_dữ_liệu",
    "Huấn_luyện_mô_hình",
    "Dự_báo_khai_thác",
    "Người_dùng",
    "Phân_quyền"
  ]

  get privilege(){
    return this.fg.get('privilege') as FormArray
  }

  addRole(){
    for (var i in this.privilegeList){
      this.privilege.push(new FormControl(this.privilegeList[i]))
    }
  }
  removePrivilege(index: any){
    this.privilege.removeAt(index)
  }

  filteredOptions!: Observable<any[]>;
  options!: any[];
  list() {
    this.quyenService.getDacQuyen().subscribe((data) => {
      this.options = data.map((Data: any) => {
        return {
          ten: Data.name,
        };
      });
      this.filteredOptions = this.fg.controls.privilege.valueChanges.pipe(
        startWith(''),
        map((value) => this.filter(value))
      );
    });
  }
  changeOption(name: any) {
    this.fg.patchValue({
      previlege: name
    });
  }
  private filter(value: any): any[] {
    const filterValue = value;
    return this.options.filter((option) =>
      option.ten.includes(filterValue)
    );
  }





  userOption!: Observable<any[]>
  option2!: any[]
  listUser(){
    this.userService.getUser().subscribe((data) => {
      this.option2 = data[0].map((Data: any) => {
        return {
          tenUser: Data.username,
          roleUser: Data.role
        };
      });
      this.userOption = this.fg.controls.name.valueChanges.pipe(
        startWith(''),
        map((value) => this.Userfilter(value))
      )
    })
  }
  changeUserOption(name: any, role: any) {
    this.fg.patchValue({
      name: name,
      mo_ta: role
    });
  }
  private Userfilter(value: any): any[] {
    const filterValue = value;
    return this.option2.filter((option) =>
      option.tenUser.includes(filterValue)
    );
  }

  ngOnInit(): void {
    this.list()
    this.listUser()
  }

  actionDialog(){
    this.dialogRef.close({
      action: this.action,
      data: this.fg.value
    })
  }

  closeDialog(){
    this.dialogRef.close({ event:'Hủy Bỏ' })
  }

}
