import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PhanQuyenComponent } from './phan-quyen.component';
import { ActionPrivilegeComponent } from './action-privilege/action-privilege.component';

const routes: Routes = [
  { path: '', component: PhanQuyenComponent },
  { path: 'action-privilege', component: ActionPrivilegeComponent},
  { path: 'action-privilege/:name', component: ActionPrivilegeComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhanQuyenRoutingModule { }
