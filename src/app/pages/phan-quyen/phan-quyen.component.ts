import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { PhanQuyenService } from 'src/app/core/services/phan-quyen.service';
import Swal from 'sweetalert2';
import { PhanQuyenDialogComponent } from './phan-quyen-dialog/phan-quyen-dialog.component';

@Component({
  selector: 'app-phan-quyen',
  templateUrl: './phan-quyen.component.html',
  styleUrls: ['./phan-quyen.component.scss']
})
export class PhanQuyenComponent implements OnInit {
  // dataSource = new MatTableDataSource<any>();
  roleList: any;
  p: any
  @ViewChild('paginator') paginator: any;
  @ViewChild('matSort') matSort: any
  constructor(
    public dialog: MatDialog,
    private QuyenService: PhanQuyenService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.getAllRole()
  }

  displayedColumns: string[] = [
    'Số thứ tự',
    'Name',
    // 'Desc',
    'Privilege',/*
    'Status', */
    'Action'
  ]

  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  //   this.dataSource.sort = this.matSort;
  // }
  getAllRole(){
    this.QuyenService.getAllQuyen().subscribe(data=>{
      this.roleList = data
    })
  }
  openToPage(){
    this.router.navigate([`/admin/phan-quyen/action-privilege/`])
  }
  redirectToPage(name: any){
    this.router.navigate([`/admin/phan-quyen/action-privilege/${name}`])
  }

  openDialog(action: any, data: any){
    const dialogRef = this.dialog.open(PhanQuyenDialogComponent, {
      width: '500px',
      height: '500px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(!result)
        return
      if(result.action == 'Thêm'){
        let add_data = {
          "name": result.data.name,
          "mo_ta": result.data.mo_ta,
          "privilege": result.data.privilege,
          "status": true
        }
        this.addQuyen(add_data)
      }
      else if(result.action == 'Cập Nhật'){
        let update_data = {
          "name": result.data.name,
          "mo_ta": result.data.mo_ta,
          "privilege": result.data.privilege,
          "status": true
        }
        this.updateQuyen(update_data, data._id)
      }
    })
  }

  addQuyen(quyen: any){
    this.QuyenService.addQuyen(quyen).subscribe(_data => {
      Swal.fire({
        title: 'Thêm phân quyền thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.getAllRole()
    })
  }

  updateQuyen(quyen: any, id: string){
    this.QuyenService.updateQuyen(quyen, id).subscribe(_data => {
      Swal.fire({
        title: 'Cập nhật phân quyền thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.getAllRole()
    })
  }

  deleteQuyen(id: string){
    Swal.fire({
      title: 'Bạn chắc chắn muốn xóa ?',
      text: "Thông tin sẽ bị xóa và không thể quay trở lại !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Xóa',
      cancelButtonText:'Hủy'
    }).then((result) => {
      if(result.isConfirmed){
        this.QuyenService.deleteQuyen(id).subscribe(() => {
          Swal.fire(
            'Thành công!',
            'Thông tin đã được xóa',
            'success'
          )
          this.getAllRole()
        })
      }
    })
  }
}
