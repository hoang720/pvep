import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhanQuyenRoutingModule } from './phan-quyen-routing.module';
import { PhanQuyenComponent } from './phan-quyen.component';
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from "@angular/material/input";
import { PhanQuyenDialogComponent } from './phan-quyen-dialog/phan-quyen-dialog.component';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { NgxPaginationModule } from 'ngx-pagination';
import { ActionPrivilegeComponent } from './action-privilege/action-privilege.component';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    PhanQuyenComponent,
    PhanQuyenDialogComponent,
    ActionPrivilegeComponent
  ],
  imports: [
    CommonModule,
    PhanQuyenRoutingModule,
    FormsModule,ReactiveFormsModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatAutocompleteModule,
    NgxPaginationModule,
    DragDropModule
  ]
})
export class PhanQuyenModule { }
