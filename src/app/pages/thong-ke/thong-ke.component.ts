import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartComponent } from "ng-apexcharts";
import { ThongKeService } from "../../core/services/thong-ke.service"



@Component({
  selector: 'app-thong-ke',
  templateUrl: './thong-ke.component.html',
  styleUrls: ['./thong-ke.component.scss']
})

export class ThongKeComponent implements OnInit {
  @ViewChild('chart', { static: true }) chart!: ChartComponent;
  sizeTotal: any;
  countTotal: any;

  linechartDataSize: any;
  linechartDataTime: any;

  ColchartDataSize: any;
  ColchartDataTime: any;

  public chartOptions!: Partial<any>;
  public chartColumnOptions!: Partial<any>;
  constructor(
    private thongKeService: ThongKeService
  ) {}

  ngOnInit(): void {
    this.getSizeTotal()
    this.getCountTotal()
    this.getChart1()
    this.getChart2()
  }

  getSizeTotal(){
    this.thongKeService.sizeTotal().subscribe(data =>{
      this.sizeTotal = data[1]
    })
  }

  getCountTotal(){
    this.thongKeService.countTotal().subscribe(count =>{
      this.countTotal = count
    })
  }

  getChart1(){
    this.thongKeService.chartData().subscribe(chart1data => {
      this.linechartDataSize = chart1data[0]
      this.linechartDataTime = chart1data[1]
      this.chartOptions = {
        series: [
          { 
            name: "Test",
            data: this.linechartDataSize
          }
        ],
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: true
          }
        },
        dataLabels: {
          enabled: true
        },
        stroke: {
          curve: "straight"
        },
        title: {
          text: "Số lượng File",
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        xaxis: {
          text: "Time",
          categories: this.linechartDataTime
        }
      }
    })
  }

  getChart2(){
    this.thongKeService.filesSize().subscribe(chart2data => {
      this.ColchartDataSize = chart2data[0]
      this.ColchartDataTime = chart2data[1]
      this.chartColumnOptions = {
        series: [
          {
            name: "Test-2",
            data: this.ColchartDataSize
          }
        ],
        title: {
          text: "Dung lượng File",
          align: "left"
        },
        chart: {
          height: 350,
          type: "bar",
          events: {
            click: function(chart: any) {
              console.log(chart)
            }
          }
        },
        colors: [
          "#008FFB",
          "#00E396",
          "#FEB019",
          "#FF4560",
          "#775DD0",
          "#546E7A",
          "#26a69a",
          "#D10CE8"
        ],
        plotOptions: {
          bar: {
            columnWidth: "45%",
            distributed: true
          }
        },
        dataLabels: {
          enabled: true
        },
        legend: {
          show: false
        },
        grid: {
          show: false
        },
        xaxis: {
          categories: this.ColchartDataTime,
          labels: {
            style:{
              colors: [
                "#008FFB",
                "#00E396",
                "#FEB019",
                "#FF4560",
                "#775DD0",
                "#546E7A",
                "#26a69a",
                "#D10CE8"
              ],
              fontSize: "12px"
            }
          }
        }
      }
    })
  }
}


