import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThongKeRoutingModule } from './thong-ke-routing.module';
import { ThongKeComponent } from './thong-ke.component';
import { NgApexchartsModule } from "ng-apexcharts";

@NgModule({
  declarations: [
    ThongKeComponent
  ],
  imports: [
    CommonModule,
    ThongKeRoutingModule,
    NgApexchartsModule
  ]
})
export class ThongKeModule { }
