import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrucQuanHoaComponent } from './truc-quan-hoa.component';

const routes: Routes = [{ path: '', component: TrucQuanHoaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrucQuanHoaRoutingModule { }
