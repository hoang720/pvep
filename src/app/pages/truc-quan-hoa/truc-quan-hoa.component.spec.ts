import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrucQuanHoaComponent } from './truc-quan-hoa.component';

describe('TrucQuanHoaComponent', () => {
  let component: TrucQuanHoaComponent;
  let fixture: ComponentFixture<TrucQuanHoaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrucQuanHoaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrucQuanHoaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
