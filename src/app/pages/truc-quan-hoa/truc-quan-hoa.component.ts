import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartComponent } from "ng-apexcharts";
import { getData } from './data';
import { AgChartOptions } from 'ag-charts-community';
import Swal from 'sweetalert2';
import { TrucQuanHoaService } from '../../core/services/truc-quan-hoa.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { ImageDetailComponent } from '../data-hinh-anh/image-detail/image-detail.component';

@Component({
  selector: 'app-truc-quan-hoa',
  templateUrl: './truc-quan-hoa.component.html',
  styleUrls: ['./truc-quan-hoa.component.scss']
})
export class TrucQuanHoaComponent implements OnInit {
  @ViewChild('chart', { static: true }) chart!: ChartComponent;


  khaithacSelected = 'Well_A.xlsx'
  KhaithacList: any;
  explodeDataTDAY: any;
  explodeDataPSIA: any;
  explodeDataQSTB: any;
  linechart1: any;
  linechart2: any;
  scatterchart: any;


  welllogSelected = 'PVEP/WellLog/PVEP/2X.las';
  ListWellog: any =[];
  welllogsData: any;
  optionValue = [
    {id: 1, text: 'Biểu đồ tổng hợp'},
    {id: 2, text: 'Biểu đồ histogram'},
    {id: 3, text: 'Biểu đồ scatter'},
  ]
  selectedOptionId: number = 1
  chartSumOptions: any
  chartSum2Options: any
  chartSum3Options: any
  chartSum4Options: any
  histogramOptions: any;
  histogram2Options: any;
  histogram3Options: any;
  histogram4Options: any;
  scatterOption: any;
  scatter2Option: any;
  scatter3Option: any;
  missingOption: any
  welllogMissingData: any;


  dataSource = new MatTableDataSource<any>();
  imageName: any;
  Total: any;
  page = 1;
  page_size = 45;
  field!: Object;
  @ViewChild('paginator') paginator: any;
  displayedColumns: string[] = [
    'Name',
    'Image',
    'Type',
    'CreatedTime',
  ]

  constructor(
    private visualizationService: TrucQuanHoaService,
    public dialog: MatDialog,
  ) {

  }

  ngOnInit(): void {
    this.getKhaiThacList()
    this.getList()
    this.getAllImage()
    this.getDataList()
    this.getWelllogsData()
  }

  //muc du lieu khai thac
  getDataList(){
    this.visualizationService.getData(this.khaithacSelected).subscribe(data => {
      this.explodeDataTDAY = data["T(DAY)"]
      this.explodeDataPSIA = data["P(PSIA)"]
      this.explodeDataQSTB = data["Q(BOPD)"]
      this.linechart1 = {
        series: [
          {
            name: "P(PSIA)",
            data:  this.explodeDataPSIA
          }
        ],
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: true
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(18, 18, 243)"],
          width: 1
        },
        title: {
          text: "",
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        yaxis: {
          title: {
            text: 'P(PSIA)',
          },
        },
        xaxis: {
          title: {
            text: 'T(DAY)',
          },
          type: 'numeric',
          categories: this.explodeDataTDAY
        }
      }
      this.linechart2 = {
        series: [
          {
            name: "Q(STB/D)",
            data: this.explodeDataQSTB
          }
        ],
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: true
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(245, 17, 17)"],
          width: 1
        },
        title: {
          text: "",
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        yaxis: {
          title: {
            text: 'Q(STB/D)',
          },
        },
        xaxis: {
          title: {
            text: 'T(DAY)',
          },
          type: 'numeric',
          categories: this.explodeDataTDAY
        }
      }
      this.scatterchart = {
        series: [
          {
            name: "P(PSIA)",
            data: this.explodeDataPSIA
          },{
            name: "Q(BOPD)",
            data: this.explodeDataQSTB
          }
        ],
        title: {
          text: "",
          align: "left"
        },
        chart: {
          animations: {
            enabled: false,
            speed: 800
          },
          height: 350,
          type: 'scatter',
          zoom: {
            enabled: true,
            type: 'xy'
          }
        },
        markers: {

          size: 4
        },
        xaxis: {
          type: 'numeric',
          title: {
            text: 'T(DAY)',
          },
          tickAmount: 10,
          categories: this.explodeDataTDAY
        },
        yaxis: {
          tickAmount: 7
        }
      }
    })
  }
  getKhaiThacList(){
    this.visualizationService.getListWellog('histprod_test').subscribe(data => {
      this.KhaithacList = data
    })
  }


  //muc welllogs
  getList(){
    this.visualizationService.getListWellog('well_log_data').subscribe(Data => {
      this.ListWellog = Data
    })
  }
  formatHistogram(formatdata: any[]){
    let convertData = formatdata.map(item => {
      return {GR: item}
    })
    return convertData
  }
  formatHistogram2(formatdata: any[]){
    let convertData = formatdata.map(item => {
      return {NPHI: item}
    })
    return convertData
  }
  formatHistogram3(formatdata: any[]){
    let convertData = formatdata.map(item => {
      return {RHOB: item}
    })
    return convertData
  }
  formatHistogram4(formatdata: any[]){
    let convertData = formatdata.map(item => {
      return {RT: item}
    })
    return convertData
  }
  getWelllogsData(){
    this.visualizationService.getWelllogs(this.welllogSelected).subscribe(data => {
      this.welllogsData = data
      this.chartSumOptions = {
        series: [
          {
            name: "GR",
            data: this.welllogsData.DEPTH,
          }
        ],
        chart: {
          width: 350,
          height: 1000,
          type: "line",
          zoom: {
            enabled: true
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(35, 241, 97)"],
          width: 1
        },
        title: {
          text: "GR",
          align: "left"
        },
        grid: {
          row: {
            // colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        xaxis: {
          title: {
            text: 'Depth (m)',
          },
          type: 'numeric',
          categories: this.welllogsData.GR
        },
      }
      this.chartSum2Options = {
        series: [
          {
            name: "NPHI",
            data: this.welllogsData.DEPTH
          }
        ],
        chart: {
          width: 350,
          height: 1000,
          type: "line",
          zoom: {
            enabled: true
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(245, 17, 17)"],
          width: 1
        },
        title: {
          text: "NPHI",
          align: "left"
        },
        grid: {
          row: {
            // colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        xaxis: {
          title: {
            text: 'Depth (m)',
          },
          type: 'numeric',
          categories: this.welllogsData.NPHI
        }
      }
      this.chartSum3Options = {
        series: [
          {
            name: "RHOB",
            data: this.welllogsData.DEPTH
          }
        ],
        chart: {
          width: 350,
          height: 1000,
          type: "line",
          zoom: {
            enabled: true
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(18, 18, 243)"],
          width: 1
        },
        title: {
          text: "RHOB",
          align: "left"
        },
        grid: {
          row: {
            // colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        xaxis: {
          title: {
            text: 'Depth (m)',
          },
          type: 'numeric',
          categories: this.welllogsData.RHOB
        }
      }
      this.chartSum4Options = {
        series: [
          {
            name: "RT",
            data: this.welllogsData.DEPTH
          }
        ],
        chart: {
          width: 350,
          height: 1000,
          type: "line",
          zoom: {
            enabled: true
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(134, 4, 134)"],
          width: 1
        },
        title: {
          text: "RT",
          align: "left"
        },
        grid: {
          row: {
            // colors: ["#f3f3f3", "transparent"],
            opacity: 0.5,
            height: 10
          }
        },
        xaxis: {
          title: {
            text: 'Depth (m)',
          },
          type: 'numeric',
          categories: this.welllogsData.RT
        },
      }


      this.histogramOptions = {
        title: {
          text: '',
        },
        subtitle: {
          text: '',
        },
        data: this.formatHistogram(this.welllogsData.GR),
        series: [
          {
            type: 'histogram',
            xKey: 'GR',
            xName: 'GR',
            fill: 'blue',
            binCount: 20
          },
        ],

        legend: {
          enabled: false,
        },
        axes: [
          {
            type: 'number',
            position: 'bottom',
            title: { text: 'GR' },

          },
          {
            type: 'number',
            position: 'left',
            title: { text: 'Tần suất' },
          },
        ],
      }
      this.histogram2Options = {
        title: {
          text: '',
        },
        subtitle: {
          text: '',
        },
        data: this.formatHistogram2(this.welllogsData.NPHI),
        series: [
          {
            type: 'histogram',
            xKey: 'NPHI',
            xName: 'NPHI',
            binCount: 20,
            fill: 'red'
          },
        ],

        legend: {
          enabled: false,
        },

        axes: [
          {
            type: 'number',
            position: 'bottom',
            title: { text: 'NPHI' },
          },
          {
            type: 'number',
            position: 'left',
            title: { text: 'Tần suất' },
          },
        ],
      }
      this.histogram3Options = {
        title: {
          text: '',
        },
        subtitle: {
          text: '',
        },
        data: this.formatHistogram3(this.welllogsData.RHOB),
        series: [
          {
            type: 'histogram',
            xKey: 'RHOB',
            xName: 'RHOB',
            binCount: 20,
            fill: 'green'
          },
        ],

        legend: {
          enabled: false,
        },

        axes: [
          {
            type: 'number',
            position: 'bottom',
            title: { text: 'RHOB' },
          },
          {
            type: 'number',
            position: 'left',
            title: { text: 'Tần suất' },
          },
        ],
      }
      this.histogram4Options = {
        title: {
          text: '',
        },
        subtitle: {
          text: '',
        },
        data: this.formatHistogram4(this.welllogsData.RT),
        series: [
          {
            type: 'histogram',
            xKey: 'RT',
            xName: 'RT',
            binCount: 20,
            fill: 'purple'
          },
        ],

        legend: {
          enabled: false,
        },

        axes: [
          {
            type: 'number',
            position: 'bottom',
            title: { text: 'RT' },
          },
          {
            type: 'number',
            position: 'left',
            title: { text: 'Tần suất' },
          },
        ],
      }


      this.scatterOption = {
        series: [
          {
            name: "GR",
            data: this.welllogsData.GR,
          },
        ],
        title: {
          text: "GR",
          align: "left"
        },
        chart: {
          animations: {
            enabled: false,
            speed: 800
          },
          height: 350,
          type: 'scatter',
          zoom: {
            enabled: true,
            type: 'xy'
          }
        },
        markers: {
          colors: ['rgb(2, 100, 2)'],
          size: 3
        },
        xaxis: {
          type: 'numeric',
          title: {
            text: 'NPHI (v/v)',
          },
          tickAmount: 10,
          categories: this.welllogsData.NPHI
        },
        yaxis: {
          tickAmount: 7
        }
      }
      this.scatter2Option = {
        series: [
          {
            name: "GR",
            data: this.welllogsData.RHOB
          },
        ],
        title: {
          text: "RHOB",
          align: "left"
        },
        chart: {
          animations: {
            enabled: false,
          },
          height: 350,
          type: 'scatter',
          zoom: {
            enabled: true,
            type: 'xy'
          }
        },
        markers: {
          colors: ['rgb(6, 6, 233)'],
          size: 3
        },
        xaxis: {
          type: 'numeric',
          title: {
            text: 'NPHI (v/v)',
          },
          tickAmount: 10,
          categories: this.welllogsData.NPHI
        },
        yaxis: {
          tickAmount: 7
        }
      }
      this.scatter3Option = {
        series: [
          {
            name: "GR",
            data: this.welllogsData.RT,
          },
        ],
        title: {
          text: "RT",
          align: "left"
        },
        chart: {
          animations: {
            enabled: false,
          },
          height: 350,
          type: 'scatter',
          zoom: {
            enabled: true,
            type: 'xy'
          }
        },
        markers: {
          colors: ['rgb(2, 100, 2)'],
          size: 3
        },
        xaxis: {
          type: 'numeric',
          title: {
            text: 'NPHI (v/v)',
          },
          categories: this.welllogsData.NPHI
        },
        yaxis: {
          tickAmount: 7
        }
      }
    })
  }
  FilterChart(){
    this.visualizationService.getFilterWelllogs(this.welllogSelected).subscribe(data => {
      this.welllogMissingData = data
      this.missingOption = {
        series: [
          {
            name: "Missing Data",
            data: [1144, 3355, 2241, 1167, 1122, 1143, 1121, 1149]
          },
          {
            name: "No Missing Data",
            data: [1113, 1123, 1120, 1118, 1113, 1127, 1133, 1112]
          }
        ],
        chart: {
          type: "bar",
          height: 350,
          width: 1590,
          stacked: true,
          stackType: "100%"
        },
        responsive: [
          {
            breakpoint: 480,
            options: {
              legend: {
                position: "bottom",
                offsetX: -10,
                offsetY: 0
              }
            }
          }
        ],
        xaxis: {
          type: 'numeric',
          categories: this.welllogMissingData.DEPTH
        },
        fill: {
          opacity: 1
        },
        legend: {
          position: "right",
          offsetX: 0,
          offsetY: 50
        }
      };
      this.welllogsData = null
    })
  }


  //du lieu hinh anh
  openDialog(action: any, data: any){
    const dialogRef = this.dialog.open(ImageDetailComponent, {
      width: '900px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(!result)
      return
      if(result.action == 'View'){
        this.getImageName(this.imageName)
      }
    })
  }

  getAllImage(){
    this.visualizationService.getImage(this.page, this.page_size).subscribe(data => {
      this.dataSource.data = data.data
      this.Total = data.total
      this.field = {
        dataSource: this.dataSource.data,
        id: '_id',
        text: 'name',
      };
    })
  }

  getImageName(imageName: any){
    this.visualizationService.getImageName(imageName).subscribe(data => {
      this.dataSource.data = data.data
    })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}
