import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrucQuanHoaRoutingModule } from './truc-quan-hoa-routing.module';
import { TrucQuanHoaComponent } from './truc-quan-hoa.component';
import { NgApexchartsModule } from "ng-apexcharts";
import { AgChartsAngularModule } from 'ag-charts-angular';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { ImageDetailComponent } from '../data-hinh-anh/image-detail/image-detail.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [
    TrucQuanHoaComponent,
    ImageDetailComponent
  ],
  imports: [
    CommonModule,
    TrucQuanHoaRoutingModule,
    NgApexchartsModule,
    AgChartsAngularModule,
    MatAutocompleteModule,
    MatTabsModule,
    FormsModule, ReactiveFormsModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule
  ]
})
export class TrucQuanHoaModule { }
