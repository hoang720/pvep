import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TruyVanDataComponent } from './truy-van-data.component';

const routes: Routes = [{ path: '', component: TruyVanDataComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TruyVanDataRoutingModule { }
