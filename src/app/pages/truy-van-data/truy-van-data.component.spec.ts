import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TruyVanDataComponent } from './truy-van-data.component';

describe('TruyVanDataComponent', () => {
  let component: TruyVanDataComponent;
  let fixture: ComponentFixture<TruyVanDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TruyVanDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TruyVanDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
