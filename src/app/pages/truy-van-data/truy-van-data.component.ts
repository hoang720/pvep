import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { TruyVanService } from '../../core/services/truy-van.service'
@Component({
  selector: 'app-truy-van-data',
  templateUrl: './truy-van-data.component.html',
  styleUrls: ['./truy-van-data.component.scss']
})
export class TruyVanDataComponent implements OnInit {
  dataSource = new MatTableDataSource<any>();
  queryStr = 'select * from well_log_data';
  page = 1;
  page_size = 10;
  constructor(
    private router: Router,
    private truyVanService: TruyVanService 
  ) {}

  @ViewChild('paginator') paginator: any;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.QueryData()
  }
  totalElements = 0

  changeData(change: any){
    this.queryStr = change
  }

  displayedColumns: string[] = []

  QueryData(){
    this.truyVanService.getQuery(this.queryStr, this.page, this.page_size).subscribe(data => {
      this.dataSource.data = data[1]
      this.displayedColumns = data[0]
      this.totalElements = data[2]
    })
  }
  
}
