import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TruyVanDataRoutingModule } from './truy-van-data-routing.module';
import { TruyVanDataComponent } from './truy-van-data.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog'
import { FormsModule, ReactiveFormsModule } from "@angular/forms"; 
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [
    TruyVanDataComponent
  ],
  imports: [
    CommonModule,
    TruyVanDataRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule, ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule
  ]
})
export class TruyVanDataModule { }
