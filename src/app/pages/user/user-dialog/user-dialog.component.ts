import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { NguoiDungService } from 'src/app/core/services/nguoi-dung.service';
import { map, startWith } from 'rxjs/operators';
import { PhanQuyenService } from 'src/app/core/services/phan-quyen.service';


@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss']
})
export class UserDialogComponent implements OnInit {
  action: string;
  userformdata: any;
  fg: FormGroup

  constructor(
    private userService: NguoiDungService,
    private quyenService: PhanQuyenService,
    public dialogRef: MatDialogRef<UserDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA)
    public data: any,
    fb: FormBuilder
  ) {
    this.action = this.data.action;
    this.userformdata = this.data.data;
    this.fg = fb.group({
      username: new FormControl( this.userformdata.username, [Validators.required] ),
      password: new FormControl( this.userformdata.password, [Validators.required] ),
      fullname: new FormControl( this.userformdata.fullname, [Validators.required] ),
      avata: [null],
      birday: new FormControl( this.userformdata.birday ),
      gender: new FormControl( this.userformdata.gender ),
      sdt: new FormControl( this.userformdata.sdt, [Validators.required] ),
      email: new FormControl( this.userformdata.email, [Validators.required] ),
      role: new FormControl( this.userformdata.role, [Validators.required] ),
      status: new FormControl( this.userformdata.status ),
    })
  }

  ngOnInit(): void {
    this.list()
  }


  uploadFile(event: any) {
    const file = event.target.files[0];
    this.fg.patchValue({
      avata: file,
    });
    this.fg.get('avata')
  }

  roleOption!: Observable<any[]>;
  options!: any[];
  list(){
    this.quyenService.getAllQuyen().subscribe((data =>
      {
        this.options = data.map((Data: any) => {
          return {
            quyen: Data.name
          };
        });
        this.roleOption = this.fg.controls.role.valueChanges.pipe(
          startWith(''),
          map((value) => this.Userfilter(value))
        );
      }
    ));
  }
  private Userfilter(value: any): any[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.quyen.toLowerCase().includes(filterValue));
  }
  changeOption(name: any) {
    this.fg.patchValue({
      role: name
    });
  }



  actionDialog(){
    this.dialogRef.close({action: this.action, data: this.fg.value})
  }

  closeDialog(){
    this.dialogRef.close({ event:'Hủy Bỏ' })
  }
}
