import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { NguoiDungService } from 'src/app/core/services/nguoi-dung.service';
import { UserDialogComponent } from './user-dialog/user-dialog.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  dataSource = new MatTableDataSource<any>();
  constructor(
    public dialog: MatDialog,
    private userService: NguoiDungService
  ) { }

  @ViewChild('paginator') paginator: any;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.getUser()
  }
  displayedColumns: string[] = [
    'Số thứ tự',
    'Name',
    'avatar',
    'Username',
    'Action'
  ]

  Filter(filter_data: any){
    this.dataSource.filter = filter_data.target.value.trim().toLowerCase();
  }

  getUser(){
    this.userService.getUser().subscribe(data => {
      this.dataSource.data = data[0]
    })
  }

  openDialog(action: any, data: any){
    const dialogRef = this.dialog.open(UserDialogComponent, {
      width: '550px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if(!res)
        return
      if(res.action == 'Thêm'){
        var add_user = {
          "username": res.data.username,
          "password": res.data.password,
          "fullname": res.data.fullname,
          "avata": res.data.avata,
          "birday": res.data.birday,
          "gender": res.data.gender,
          "sdt": res.data.sdt,
          "email": res.data.email,
          "role": res.data.role,
          "status": res.data.status
        }
        if(res.data.avata){
          this.userService.addAvatar(res.data.avata).subscribe((image_link) => {
            add_user.avata = image_link
            this.AddUser(
              add_user,
              add_user.username,
              add_user.password,
              add_user.fullname,
              add_user.avata,
              add_user.birday,
              add_user.gender,
              add_user.sdt,
              add_user.email,
              add_user.role,
              add_user.status
            )
          })
        }
        else{
          this.AddUser(
            add_user,
            add_user.username,
            add_user.password,
            add_user.fullname,
            add_user.avata,
            add_user.birday,
            add_user.gender,
            add_user.sdt,
            add_user.email,
            add_user.role,
            add_user.status
          )
          console.log(add_user)
        }
      }
      else if (res.action == 'Cập Nhật'){
        var update_user = {
          "username": res.data.username,
          "fullname": res.data.fullname,
          "avata": res.data.avata,
          "birday": res.data.birday,
          "gender": res.data.gender,
          "sdt": res.data.sdt,
          "email": res.data.email,
          "role": res.data.role,
          "status": res.data.status
        }
        if(res.data.avata){
          this.userService.addAvatar(res.data.avata).subscribe((image_link) => {
            update_user.avata = image_link
            this.UpdateUser(
              update_user,
              data._id,
              update_user.username,
              update_user.fullname,
              update_user.avata,
              update_user.birday,
              update_user.gender,
              update_user.sdt,
              update_user.email,
              update_user.role,
              update_user.status
            )
          })
        }else{
          this.UpdateUser(
            update_user,
            data._id,
            update_user.username,
            update_user.fullname,
            update_user.avata,
            update_user.birday,
            update_user.gender,
            update_user.sdt,
            update_user.email,
            update_user.role,
            update_user.status
          )
        }
      }
    })
  }

  AddUser(
    User: any,
    username: any,
    password: any,
    fullname: any,
    avata: any,
    birday: any,
    gender: any,
    sdt: any,
    email: any,
    role: any,
    status: any
  ){
    this.userService.addUser(User, username, password, fullname, avata, birday, gender, sdt, email, role, status).subscribe(data => {
      Swal.fire({
        title: 'Thêm thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.getUser()
    })
  }

  UpdateUser(
    User: any,
    id: string,
    username: any,
    fullname: any,
    avata: any,
    birday: any,
    gender: any,
    sdt: any,
    email: any,
    role: any,
    status: any
  ){
    this.userService.updateUser(User, id, username, fullname, avata, birday, gender, sdt, email, role, status).subscribe(data => {
      Swal.fire({
        title: 'Cập Nhật thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.getUser()
    })
  }

  deleteUser(id: string){
    Swal.fire({
      title: 'Bạn chắc chắn muốn xóa ?',
      text: "Thông tin sẽ bị xóa và không thể quay trở lại !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Xóa',
      cancelButtonText:'Hủy'
    }).then((result) => {
      if(result.isConfirmed){
        this.userService.deleteUser(id).subscribe(()=>{
          Swal.fire(
            'Thành công!',
            'Thông tin đã được xóa',
            'success'
          )
          this.getUser()
        })
      }
    })
  }
}
