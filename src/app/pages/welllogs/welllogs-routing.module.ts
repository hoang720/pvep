import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelllogsComponent } from './welllogs.component';

const routes: Routes = [{ path: '', component: WelllogsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelllogsRoutingModule { }
