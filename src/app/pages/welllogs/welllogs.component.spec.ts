import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WelllogsComponent } from './welllogs.component';

describe('WelllogsComponent', () => {
  let component: WelllogsComponent;
  let fixture: ComponentFixture<WelllogsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WelllogsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WelllogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
