import { Component, OnInit } from '@angular/core';
import { AgChartOptions } from 'ag-charts-community';
import { TrucQuanHoaService } from 'src/app/core/services/truc-quan-hoa.service';
import * as Highcharts from 'highcharts';


@Component({
  selector: 'app-welllogs',
  templateUrl: './welllogs.component.html',
  styleUrls: ['./welllogs.component.scss']
})
export class WelllogsComponent implements OnInit {

  chartSumOptions: any
  chartSum2Options: any
  chartSum3Options: any
  chartSum4Options: any
  histogramOptions: any;
  histogram2Options: any;
  histogram3Options: any;
  histogram4Options: any;
  scatterOption: any;
  scatter2Option: any;
  scatter3Option: any;
  missingOption: any

  welllogsData: any;
  welllogsList: any;
  scatterData: any;
  histogramData: any;
  sumData: any;
  welllogMissingData: any;
  welllogSelected2 = 'PVEP/WellLog/PVEP/2X.las';

  highcharts = Highcharts;
  chartOptions: any 


  constructor(
    private welllogService: TrucQuanHoaService,
  ) {}

  ngOnInit(): void {
    this.getWelllogsData()
    this.getList()
  }

  formatHistogram(formatdata: any[]){
    let convertData = formatdata.map(item => {
      return {GR: item}
    })
    return convertData
  }

  formatHistogram2(formatdata: any[]){
    let convertData = formatdata.map(item => {
      return {NPHI: item}
    })
    return convertData
  }
  formatHistogram3(formatdata: any[]){
    let convertData = formatdata.map(item => {
      return {RHOB: item}
    })
    return convertData
  }
  formatHistogram4(formatdata: any[]){
    let convertData = formatdata.map(item => {
      return {RT: item}
    })
    return convertData
  }

  optionValue = [
    {id: 1, text: 'Biểu đồ tổng hợp'},
    {id: 2, text: 'Biểu đồ histogram'},
    {id: 3, text: 'Biểu đồ scatter'},
  ]

  selectedOptionId: number = 1

  getWelllogsData(){
    this.welllogService.getWelllogs(this.welllogSelected2).subscribe(data => {
      this.welllogsData = data
      this.chartSumOptions = {
        series: [
          { 
            name: "Test",
            data: this.welllogsData.GR,
          }
        ],
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: true
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(35, 241, 97)"],
          width: 1
        },
        title: {
          text: "GR",
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        xaxis: {
          title: {
            text: 'Depth (m)',
          },
          type: 'numeric',
          categories: this.welllogsData.DEPTH
        },
      }
      this.chartSum2Options = {
        series: [
          { 
            name: "Test",
            data: this.welllogsData.NPHI
          }
        ],
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: true
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(245, 17, 17)"],
          width: 1
        },
        title: {
          text: "NPHI",
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        xaxis: {
          title: {
            text: 'Depth (m)',
          },
          type: 'numeric',
          categories: this.welllogsData.DEPTH
        }
      }
      this.chartSum3Options = {
        series: [
          { 
            name: "Test",
            data: this.welllogsData.RHOB
          }
        ],
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: true
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(18, 18, 243)"],
          width: 1
        },
        title: {
          text: "RHOB",
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        xaxis: {
          title: {
            text: 'Depth (m)',
          },
          type: 'numeric',
          categories: this.welllogsData.DEPTH
        }
      }
      this.chartSum4Options = {
        series: [
          { 
            name: "Test",
            data: this.welllogsData.RT
          }
        ],
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: true
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight",
          colors: ["rgb(134, 4, 134)"],
          width: 1
        },
        title: {
          text: "RT",
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"],
            opacity: 0.5
          }
        },
        xaxis: {
          title: {
            text: 'Depth (m)',
          },
          type: 'numeric',
          categories: this.welllogsData.DEPTH
        },
      }
    
    
      this.histogramOptions = {
        title: {
          text: '',
        },
        subtitle: {
          text: '',
        },
        data: this.formatHistogram(this.welllogsData.GR),
        series: [
          {
            type: 'histogram',
            xKey: 'GR',
            xName: 'GR',
            fill: 'blue',
            binCount: 20
          },
        ],
        
        legend: {
          enabled: false,
        },
        axes: [
          {
            type: 'number',
            position: 'bottom',
            title: { text: 'GR' },

          },
          {
            type: 'number',
            position: 'left',
            title: { text: 'Tần suất' },
          },
        ],
      }
      this.histogram2Options = {
        title: {
          text: '',
        },
        subtitle: {
          text: '',
        },
        data: this.formatHistogram2(this.welllogsData.NPHI),
        series: [
          {
            type: 'histogram',
            xKey: 'NPHI',
            xName: 'NPHI',
            binCount: 20,
            fill: 'red'
          },
        ],
        
        legend: {
          enabled: false,
        },
        
        axes: [
          {
            type: 'number',
            position: 'bottom',
            title: { text: 'NPHI' },
          },
          {
            type: 'number',
            position: 'left',
            title: { text: 'Tần suất' },
          },
        ],
      }
      this.histogram3Options = {
        title: {
          text: '',
        },
        subtitle: {
          text: '',
        },
        data: this.formatHistogram3(this.welllogsData.RHOB),
        series: [
          {
            type: 'histogram',
            xKey: 'RHOB',
            xName: 'RHOB',
            binCount: 20,
            fill: 'green'
          },
        ],
        
        legend: {
          enabled: false,
        },
        
        axes: [
          {
            type: 'number',
            position: 'bottom',
            title: { text: 'RHOB' },
          },
          {
            type: 'number',
            position: 'left',
            title: { text: 'Tần suất' },
          },
        ],
      }
      this.histogram4Options = {
        title: {
          text: '',
        },
        subtitle: {
          text: '',
        },
        data: this.formatHistogram4(this.welllogsData.RT),
        series: [
          {
            type: 'histogram',
            xKey: 'RT',
            xName: 'RT',
            binCount: 20,
            fill: 'purple'
          },
        ],
        
        legend: {
          enabled: false,
        },
        
        axes: [
          {
            type: 'number',
            position: 'bottom',
            title: { text: 'RT' },
          },
          {
            type: 'number',
            position: 'left',
            title: { text: 'Tần suất' },
          },
        ],
      }
    
    
      this.scatterOption = {
        series: [
          {
            name: "GR",
            data: this.welllogsData.GR,
          },
        ],
        title: {
          text: "GR",
          align: "left"
        },
        chart: {
          animations: {
            enabled: false,
            speed: 800
          },
          height: 350,
          type: 'scatter',
          zoom: {
            enabled: true,
            type: 'xy'
          }
        },
        markers: {
          colors: ['rgb(2, 100, 2)'],
          size: 3
        },
        xaxis: {
          type: 'numeric',
          title: {
            text: 'NPHI (v/v)',
          },
          tickAmount: 10,
          categories: this.welllogsData.NPHI
        },
        yaxis: {
          tickAmount: 7
        }
      }
      this.scatter2Option = {
        series: [
          {
            name: "GR",
            data: this.welllogsData.RHOB
          },
        ],
        title: {
          text: "RHOB",
          align: "left"
        },
        chart: {
          animations: {
            enabled: false,
          },
          height: 350,
          type: 'scatter',
          zoom: {
            enabled: true,
            type: 'xy'
          }
        },
        markers: {
          colors: ['rgb(6, 6, 233)'],
          size: 3
        },
        xaxis: {
          type: 'numeric',
          title: {
            text: 'NPHI (v/v)',
          },
          tickAmount: 10,
          categories: this.welllogsData.NPHI
        },
        yaxis: {
          tickAmount: 7
        }
      }
      this.scatter3Option = {
        series: [
          {
            name: "GR",
            data: this.welllogsData.RT,
          },
        ],
        title: {
          text: "RT",
          align: "left"
        },
        chart: {
          animations: {
            enabled: false,
          },
          height: 350,
          type: 'scatter',
          zoom: {
            enabled: true,
            type: 'xy'
          }
        },
        markers: {
          colors: ['rgb(2, 100, 2)'],
          size: 3
        },
        xaxis: {
          type: 'numeric',
          title: {
            text: 'NPHI (v/v)',
          },
          categories: this.welllogsData.NPHI
        },
        yaxis: {
          tickAmount: 7
        }
      }
      console.log(this.welllogsData)
    })
  }

  getList(){
    this.welllogService.getListWellog('well_log_data').subscribe(data => {
      this.welllogsList = data
    })
  }

  FilterChart(){
    this.welllogService.getFilterWelllogs(this.welllogSelected2).subscribe(data => {
      this.welllogMissingData = data
      this.missingOption = {
        series: [
          {
            name: "Missing Data",
            data: [1144, 3355, 2241, 1167, 1122, 1143, 1121, 1149]
          },
          {
            name: "No Missing Data",
            data: [1113, 1123, 1120, 1118, 1113, 1127, 1133, 1112]
          }
        ],
        chart: {
          type: "bar",
          height: 350,
          width: 1590,
          stacked: true,
          stackType: "100%"
        },
        responsive: [
          {
            breakpoint: 480,
            options: {
              legend: {
                position: "bottom",
                offsetX: -10,
                offsetY: 0
              }
            }
          }
        ],
        xaxis: {
          type: 'numeric',
          categories: this.welllogMissingData.DEPTH
        },
        fill: {
          opacity: 1
        },
        legend: {
          position: "right",
          offsetX: 0,
          offsetY: 50
        }
      };
      this.welllogsData = null
    })
  }

  getScatter(){
    this.welllogService.getScatterChart(this.welllogSelected2).subscribe(data => {
      this.scatterData = data
    })
  }

  getHistogram(){
    this.welllogService.getHistogramChart(this.welllogSelected2).subscribe(data => {
      this.histogramData = data
    })
  }

  getSum(){
    this.welllogService.getSumChart(this.welllogSelected2).subscribe(data => {
      this.sumData = data
    })
  }
}
