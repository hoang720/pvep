import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelllogsRoutingModule } from './welllogs-routing.module';
import { WelllogsComponent } from './welllogs.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormsModule } from '@angular/forms';
import { NgApexchartsModule } from "ng-apexcharts";
import { AgChartsAngularModule } from 'ag-charts-angular';
import { ChartModule } from '@syncfusion/ej2-angular-charts';
import { HighchartsChartModule } from 'highcharts-angular';

@NgModule({
  declarations: [
    WelllogsComponent
  ],
  imports: [
    CommonModule,
    WelllogsRoutingModule,
    MatAutocompleteModule,
    FormsModule,
    NgApexchartsModule,
    AgChartsAngularModule,
    ChartModule,
    HighchartsChartModule,
  ]
})
export class WelllogsModule { }
